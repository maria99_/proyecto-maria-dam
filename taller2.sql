-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-05-2021 a las 11:29:16
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `taller2`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `IdCliente` int(11) NOT NULL,
  `RFC` int(10) NOT NULL,
  `Nombre` varchar(60) NOT NULL,
  `Direccion` varchar(60) NOT NULL,
  `tel` varchar(10) DEFAULT NULL,
  `CP` int(10) NOT NULL,
  `Poblacion` varchar(45) NOT NULL,
  `DNI` varchar(10) NOT NULL,
  `Email` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`IdCliente`, `RFC`, `Nombre`, `Direccion`, `tel`, `CP`, `Poblacion`, `DNI`, `Email`) VALUES
(2, 34223, 'Antonio', 'Calle Oeste', '6378234', 50067, 'Malaga', '63739287K', 'pedro@gmail.com'),
(3, 31, 'dfs', 'sfsd', '232', 23, 'fsf', '232', 'fsd'),
(9, 43, 'dsf', 'fsd', '43', 3432, 'dsf', '34', 'dsff'),
(10, 545, 'pepe', 'jcoisdjfo', '453', 54, 'sdefd', '423', 'dsfd'),
(34, 3432, 'sasa', 'fsd', '43', 344, 'dfs', '32', 'dsfds'),
(45335, 5435, 'hola', 'fdfdsf', '54334', 345, 'gffddfg', '34', 'fdgd');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `IdFactura` int(11) NOT NULL,
  `IdCliente` int(11) NOT NULL,
  `Idhoja` int(10) NOT NULL,
  `IdMec` int(10) NOT NULL,
  `Idvehiculo` int(10) NOT NULL,
  `Nofact` int(10) NOT NULL,
  `Fecha` date NOT NULL,
  `impuesto` decimal(2,0) NOT NULL,
  `RFC` int(10) NOT NULL,
  `NombreCliente` varchar(20) NOT NULL,
  `DNI` varchar(10) NOT NULL,
  `Total` decimal(20,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `factura`
--

INSERT INTO `factura` (`IdFactura`, `IdCliente`, `Idhoja`, `IdMec`, `Idvehiculo`, `Nofact`, `Fecha`, `impuesto`, `RFC`, `NombreCliente`, `DNI`, `Total`) VALUES
(1, 34, 3, 2, 1, 54, '2021-05-29', '20', 34223, 'Maria', '55545H', '2839'),
(2, 45335, 12, 2, 1, 34, '2021-05-20', '54', 34223, 'jopse', '34324765Ñ', '685'),
(3, 3, 12, 2, 1, 43, '2021-05-29', '43', 765, 'jose maria', '5435J', '9383'),
(4, 45335, 2, 54, 6, 34, '2021-05-30', '10', 876, 'Lola', '76488F', '234'),
(5, 45335, 12, 2, 4, 98, '2021-06-26', '20', 85494, 'Sergio', '75649L', '987'),
(6, 3, 12, 7, 6, 342, '2021-06-26', '10', 65, 'Paula', '65464', '874');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hojap`
--

CREATE TABLE `hojap` (
  `Idhoja` int(10) NOT NULL,
  `IdFac` int(10) NOT NULL,
  `Idmec` int(10) NOT NULL,
  `concepto` varchar(100) NOT NULL,
  `Cantidad` int(11) NOT NULL,
  `Solicitante` varchar(20) NOT NULL,
  `FechaPrevistaEntrega` date NOT NULL,
  `Reparacion` int(10) NOT NULL,
  `NombreProveedor` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `hojap`
--

INSERT INTO `hojap` (`Idhoja`, `IdFac`, `Idmec`, `concepto`, `Cantidad`, `Solicitante`, `FechaPrevistaEntrega`, `Reparacion`, `NombreProveedor`) VALUES
(2, 65, 3, 'Chasis', 7543, 'Administrador', '2021-05-31', 1, 'Taller Renault'),
(3, 6, 7, 'Paragolpes', 847, 'Mecanico', '2021-05-28', 3, 'Taller Opel'),
(12, 23, 1, 'fsfdsf', 3, 'dfsf', '2021-05-14', 1, 'sdf');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mecanico`
--

CREATE TABLE `mecanico` (
  `Idmec` int(10) NOT NULL,
  `IdFac` int(10) NOT NULL,
  `Nombre` varchar(60) NOT NULL,
  `Apellido` varchar(10) NOT NULL,
  `Direccion` varchar(60) NOT NULL,
  `tel` varchar(10) DEFAULT NULL,
  `Costoxhora` decimal(2,0) NOT NULL,
  `MatriculaCoche` varchar(10) NOT NULL,
  `HorasTrabajadas` decimal(4,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `mecanico`
--

INSERT INTO `mecanico` (`Idmec`, `IdFac`, `Nombre`, `Apellido`, `Direccion`, `tel`, `Costoxhora`, `MatriculaCoche`, `HorasTrabajadas`) VALUES
(2, 2989, 'Antonio', 'matamorors', 'Plaza lopez', '6378234', '23', '7832YGD', '12'),
(3, 65, 'jose ', 'Alon', 'Plaza lopez', '839245', '21', '7832YGD', '12'),
(4, 29, 'maria', 'lopez', 'Calle Oeste', '6378234', '21', '8438HDG', '7'),
(7, 4, 'dfs', 'matamorors', 'dasd', '43', '43', '43', '0'),
(54, 7, 'ana', 'garcia', 'gbfgsf', '54556', '23', '554hjf', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `repuesto`
--

CREATE TABLE `repuesto` (
  `Idrep` int(10) NOT NULL,
  `IdHojap` int(10) NOT NULL,
  `Descripción` varchar(60) NOT NULL,
  `CostoUnico` decimal(2,0) NOT NULL,
  `PrecioUnico` decimal(2,0) NOT NULL,
  `NombrePieza` varchar(10) NOT NULL,
  `CantidadUsada` int(4) NOT NULL,
  `Solicitante` varchar(20) NOT NULL,
  `Proveedor` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `repuesto`
--

INSERT INTO `repuesto` (`Idrep`, `IdHojap`, `Descripción`, `CostoUnico`, `PrecioUnico`, `NombrePieza`, `CantidadUsada`, `Solicitante`, `Proveedor`) VALUES
(1, 3, 'gel', '33', '6', '', 5, 'Mecanico', 'Renault'),
(2, 54, 'Guantes', '9', '4', 'Guantes', 754, 'Mecanico', 'Renault'),
(3, 4, 'das', '3', '3', 'das', 4, 'das', 'da'),
(4, 1, 'Ruedas', '65', '85', 'Ruedas', 982, 'Mecanico', 'Renault'),
(5, 4, 'aceite', '5', '65', 'aceite', 65, 'mecanico', 'renault');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehiculo`
--

CREATE TABLE `vehiculo` (
  `IdVehiculo` int(11) NOT NULL,
  `Matricula` varchar(10) NOT NULL,
  `Modelo` varchar(60) NOT NULL,
  `Color` varchar(60) DEFAULT NULL,
  `Fecha_ent` date NOT NULL,
  `Hora_ent` time NOT NULL,
  `Kilometros` decimal(20,0) NOT NULL,
  `RFC` int(10) NOT NULL,
  `Mec` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `vehiculo`
--

INSERT INTO `vehiculo` (`IdVehiculo`, `Matricula`, `Modelo`, `Color`, `Fecha_ent`, `Hora_ent`, `Kilometros`, `RFC`, `Mec`) VALUES
(1, '8380920H', 'Mercedes', 'Negro', '2021-05-20', '16:26:24', '23221', 34223, 2),
(2, '846756L', 'Audi', 'Blanco', '2021-05-28', '16:26:24', '5345', 31, 3),
(4, '656', 'fsdf', 'fsf', '2021-05-08', '02:00:00', '54', 65, 6),
(5, '656', 'fsdf', 'dsf', '2021-05-16', '01:30:00', '4', 6, 6),
(6, '65345', 'fdsf', 'fsdf', '2021-05-08', '01:00:00', '5667', 5, 7),
(54, '545', 'fdf', 'dsf', '2021-05-14', '01:30:00', '432', 43, 4);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`IdCliente`),
  ADD KEY `RFC` (`RFC`);

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`IdFactura`),
  ADD KEY `IdCliente` (`IdCliente`),
  ADD KEY `Idhoja` (`Idhoja`),
  ADD KEY `IdMec` (`IdMec`),
  ADD KEY `IdVehiculo` (`Idvehiculo`);

--
-- Indices de la tabla `hojap`
--
ALTER TABLE `hojap`
  ADD PRIMARY KEY (`Idhoja`),
  ADD KEY `Idmec` (`Idmec`),
  ADD KEY `Reparacion` (`Reparacion`),
  ADD KEY `IdFac` (`IdFac`);

--
-- Indices de la tabla `mecanico`
--
ALTER TABLE `mecanico`
  ADD PRIMARY KEY (`Idmec`),
  ADD KEY `IdFac` (`IdFac`);

--
-- Indices de la tabla `repuesto`
--
ALTER TABLE `repuesto`
  ADD PRIMARY KEY (`Idrep`),
  ADD KEY `IdHojap` (`IdHojap`);

--
-- Indices de la tabla `vehiculo`
--
ALTER TABLE `vehiculo`
  ADD PRIMARY KEY (`IdVehiculo`),
  ADD KEY `RFC` (`RFC`),
  ADD KEY `Mec` (`Mec`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `factura_ibfk_1` FOREIGN KEY (`IdCliente`) REFERENCES `cliente` (`IdCliente`),
  ADD CONSTRAINT `factura_ibfk_2` FOREIGN KEY (`Idhoja`) REFERENCES `hojap` (`Idhoja`),
  ADD CONSTRAINT `factura_ibfk_3` FOREIGN KEY (`IdMec`) REFERENCES `mecanico` (`Idmec`),
  ADD CONSTRAINT `factura_ibfk_4` FOREIGN KEY (`Idvehiculo`) REFERENCES `vehiculo` (`IdVehiculo`);

--
-- Filtros para la tabla `hojap`
--
ALTER TABLE `hojap`
  ADD CONSTRAINT `hojap_ibfk_2` FOREIGN KEY (`Reparacion`) REFERENCES `repuesto` (`Idrep`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
