package com.mariava.taller.VistaUsuarios;

import com.mariava.taller.Clases.Cliente;
import com.mariava.taller.Factura.JasperReport;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Clase que se accede como cliente
 */
public class VistaCliente {

    // Atributos
    public JLabel txtEstado;
    public JPanel panelInforme;
    public JPanel panelCliente;

    // Menu con opciones
    public JMenuItem salirItem;

    // Atributos
    private JFrame frame;
    private JPanel panel1;
    public JTabbedPane tabbedPane1;
    public JButton generarFacturaClienteButton;
    public JTextField txtNombreCliente;
    public JTextField txtDniCliente;
    public JButton modificarClienteButton;
    public JButton listarClienteButton;

    public JList listCliente;
    public JList<Cliente> listarCliente;
    public DefaultListModel<Cliente> dlmCliente;

    /**
     * Creacion del constructor
     */
    public VistaCliente(){
        frame = new JFrame("Cliente");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        generarFacturaClienteButton.setToolTipText("Genera informe del cliente");
        modificarClienteButton.setToolTipText("Modifica cliente");
        listarClienteButton.setToolTipText("Lista cliente");

       menuClientes();

       iniciarListaCliente();

    }

    /**
     * Iniciar la lista de clientes
     */
    private void iniciarListaCliente() {
        dlmCliente = new DefaultListModel<>();
        listCliente.setModel(dlmCliente);
    }

    /**
     * Creacion de un menu de opciones apra poder salir o conectarse
     */
    private void menuClientes() {
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Opciones");

        salirItem = new JMenuItem("Salir");
        salirItem.setActionCommand("Salir");

        menu.add(salirItem);
        barra.add(menu);
        frame.setJMenuBar(barra);
    }


}
