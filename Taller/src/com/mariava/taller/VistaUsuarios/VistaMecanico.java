package com.mariava.taller.VistaUsuarios;

import com.github.lgooddatepicker.components.DatePicker;
import com.github.lgooddatepicker.components.TimePicker;
import com.mariava.taller.Clases.Factura;
import com.mariava.taller.Clases.Mecanico;
import com.mariava.taller.Clases.Vehiculo;

import javax.swing.*;

/**
 * Clase que genera la vista de acceso a mecanico
 */
public class VistaMecanico {

    //Atributos
    private JFrame frame;
    public JLabel txtEstado;

    public JPanel panelMecanico;
    public JPanel panelVehiculo;
    public JPanel panelFactura;

    //Menu con opciones diferentes
    public JMenuItem salirItem;

    public JTabbedPane tabbedPane1;
    public JPanel panel1;

    public JTextField txtIdMec;
    public JTextField txtIdFacturaMecanico;
    public JTextField txtNombreMecanico;
    public JTextField txtApellidoMecanico;
    public JTextField txtDireccionMecanico;
    public JTextField txtTelefonoMecanico;
    public JTextField txtCostoMecanico;
    public JTextField txtMatriculaMecanico;
    public JTextField txtHorasMecanico;
    public JButton registrarMecanicoButton;
    public JButton modificarMecanicoButton;
    public JButton eliminarMecanicoButton;
    public JButton listarMecanicoButton;

    public JList listaMecanico;

    public JList<Mecanico> listMecanico;
    public DefaultListModel<Mecanico> dlmMecanico;

    // Vehiculo
    public JTextField txtIdVehiculo;
    public JTextField txtMatriculaVehiculo;
    public JTextField txtModeloVehiculo;
    public JTextField txtColorVehiculo;
    public DatePicker datePicker;
    public TimePicker timePicker;
    public JTextField txtKmVehiculo;
    public JButton registrarVehiculoButton;
    public JButton modificarVehiculoButton;
    public JButton eliminarVehiculoButton;
    public JButton listarVehiculoButton;
    public JTextField txtRfcVehiculo;
    public JTextField txtMecVehiculo;

    public JList listaVehiculo;
    public JList<Vehiculo> listVehiculo;
    public DefaultListModel<Vehiculo> dlmVehiculo;

    //Factura
    public JTextField txtIdFactura;
    public JTextField txtIDClienteFactura;
    public JTextField txtIdHojaFactura;
    public JTextField txtIdMecFactura;
    public JTextField txtIdVehiculoFactura;
    public JTextField txtNumFactura;
    public JTextField txtImpuestoFactura;
    public DatePicker datePickerFactura;
    public JTextField txtRfcFactura;
    public JTextField txtNombreFactura;
    public JTextField txtDniFacura;
    public JTextField txtTotalFactura;
    public JButton modificarFacturaButton;
    public JButton eliminarFacturaButton;
    public JButton generarFacturaButton;
    public JButton listarFacturaButton;

    public JList listarFactura;
    public JList<Factura> listFactura;
    public DefaultListModel<Factura> dlmFactura;

    /**
     * Creacion del constructor
     */
    public VistaMecanico(){
        frame = new JFrame("Mecanicos");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        //Mecanicos
        registrarMecanicoButton.setToolTipText("Registrar un mecanico");
        modificarMecanicoButton.setToolTipText("Solo puedes modificar el apellido");
        eliminarMecanicoButton.setToolTipText("Eliminar a un mecanico");
        listarMecanicoButton.setToolTipText("Listar a los mecanicos");

        //Vehiculos
        registrarVehiculoButton.setToolTipText("Registrar un vehiculo");
        modificarVehiculoButton.setToolTipText("Solo puedes modificar la matricula");
        eliminarVehiculoButton.setToolTipText("Eliminar a un vehiculo");
        listarVehiculoButton.setToolTipText("Listar a los vehiculos");

        //Factura
        // registrarFacturaButton.setToolTipText("Registrar una factura");
        modificarFacturaButton.setToolTipText("Puedes modificar nombre y dni");
        eliminarFacturaButton.setToolTipText("Eliminar una factura");
        listarFacturaButton.setToolTipText("Listar las facturas");
        generarFacturaButton.setToolTipText("Generar la factura de un cliente");

        menuMecanicos();

        inciarListaMecanicos();
        iniciarListaVehiculo();
        iniciarListaFactura();
    }

    /**
     * Creacion de un menu de opciones apra poder salir o conectarse
     */
    private void menuMecanicos() {
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Opciones");

        salirItem = new JMenuItem("Salir");
        salirItem.setActionCommand("Salir");

        menu.add(salirItem);
        barra.add(menu);
        frame.setJMenuBar(barra);
    }


    /**
     * Iniciar la lista de mecanico
     */
    private void inciarListaMecanicos() {
        dlmMecanico = new DefaultListModel<>();
        listaMecanico.setModel(dlmMecanico);
    }

    /**
     * Iniciar la lista de vehiculo
     */
    private void iniciarListaVehiculo() {
        dlmVehiculo = new DefaultListModel<>();
        listaVehiculo.setModel(dlmVehiculo);
    }

    /**
     * Iniciar la lista de factura
     */
    private void iniciarListaFactura() {
        dlmFactura = new DefaultListModel<>();
        listarFactura.setModel(dlmFactura);
    }

}
