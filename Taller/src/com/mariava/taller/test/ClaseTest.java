package com.mariava.taller.test;

import com.mariava.taller.Clases.Cliente;
import com.mariava.taller.Util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.*;


/**
 * Clase que realiza los test de JUnit
 */
public class ClaseTest {

    //Atributos de la clase
    private static SessionFactory sessionFactory;
    private Session session;

    /**
     * Crea la sesion con hibernate
     * @throws Exception
     */
    @BeforeAll
    static void setUpBeforeClass() throws Exception {
        sessionFactory = HibernateUtil.getSessionFactory();
        System.out.println("SessionFactory creada");

    }

    /**
     * Destruye la sesion
     * @throws Exception
     */
    @AfterAll
    static void tearDownAfterClass() throws Exception {
        sessionFactory.close();
        System.out.println("SessionFactory destruida");

    }

    /**
     * Crea la sesion
     * @throws Exception
     */
    @BeforeEach
    void setUp() throws Exception {
        session =  sessionFactory.openSession();
        System.out.println("Session creada");

    }

    /**
     * Cierra la sesion
     * @throws Exception
     */
    @AfterEach
    void tearDown() throws Exception {
        session.close();
        System.out.println("Session cerrada");

    }

    /**
     * Me indica si existe el cliente con ese nombre y esa id
     */
    @Test
    void testGet() {
        System.out.println("Correindo la base de datos para que me diga si esta el nombre del cliente, "
                + "tiene que salir que si");
        Integer id = 2;

        Cliente cliente = session.find(Cliente.class, id);

        Assertions.assertEquals("Antonio",cliente.getNombre());
    }

    /**
     * Metodo que me indica que el cliente con ese id no existe
     */
    @Test
    void testGetNotExist() {
        System.out.println("Correindo la base de datos para indicarme si esta o no, tiene que salir que no");

        Integer id = 1;

        Cliente cliente = session.find(Cliente.class, id);

        Assertions.assertNull(cliente);

    }

}
