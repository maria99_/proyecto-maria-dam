package com.mariava.taller.Util;

import javax.swing.*;

/**
 * Clase util, para añadir todos los metodos necesarios
 */
public class Util {

    /**
     * Metodo de error
     * @param mensaje variable llamada asi
     */
    public static void mensajeError(String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje,"Error", JOptionPane.ERROR_MESSAGE);
    }


}
