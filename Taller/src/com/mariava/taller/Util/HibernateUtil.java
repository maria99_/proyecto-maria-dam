package com.mariava.taller.Util;

import com.mariava.taller.Clases.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/**
 * Clase que hace que conecte con hibernate
 */
public class HibernateUtil {

    //Atributos
    private static SessionFactory sessionFactory;
    private static Session session;

    /**
     * Metodo que crea la factoria de sesiones
     * @return
     */
    public static SessionFactory buildSessionFactory() {
        Configuration configuration = new Configuration();
        configuration.configure();

        configuration.addAnnotatedClass(Cliente.class);
        configuration.addAnnotatedClass(Factura.class);
        configuration.addAnnotatedClass(Hojap.class);
        configuration.addAnnotatedClass(Mecanico.class);
        configuration.addAnnotatedClass(Repuesto.class);
        configuration.addAnnotatedClass(Vehiculo.class);


        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                configuration.getProperties()).build();
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);

        return sessionFactory;
    }

    /**
     * Abre una nueva sesion
     * @return
     */
    public static Session openSession() {
        session = sessionFactory.openSession();
        return null;
    }

    /**
     * Devuelve la sesion actual
     * @return
     */
    public static Session getCurrentSession() {

        if ((session == null) || (!session.isOpen()))
            openSession();

        return session;
    }

    /**
     * Cierra hibernate
     */
    public static void closeSessionFactory() {

        if (session != null)
            session.close();

        if (sessionFactory != null)
            sessionFactory.close();
    }

    /**
     * Devuelve laq sesion de hibernate
     * @return
     */
    public static SessionFactory getSessionFactory() {
        if(sessionFactory == null) sessionFactory = buildSessionFactory();
        return sessionFactory;
    }

}
