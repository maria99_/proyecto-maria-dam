package com.mariava.taller.Factura;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

import javax.swing.*;
import java.util.HashMap;

/**
 * Clase que hace la consulta con jasperreport
 */
public class ReportGenerator {

    //Atributo
    public static final String FACTURAS = "facturasTaller.jasper";

    /**
     * Metodo que genera el infrome, con el parametro que quiero y manda un mensaje, para que escribas
     * la factura del cliente que quieras
     * @return
     */
    public static JasperPrint generarInformeFactura() {
        HashMap<String, Object> parametros = new HashMap<>();

        String cliente = JOptionPane.showInputDialog(null, "Introduce el nombre del cliente para generar la factura");

        parametros.put("parametro_nombre", cliente);
        try {
            JasperPrint informeLleno = JasperFillManager.fillReport(FACTURAS, parametros, ConexionVicente.getMySQLConnection());
            return informeLleno;
        }catch(JRException e) {
            e.printStackTrace();
        }
        return null;

    }
}
