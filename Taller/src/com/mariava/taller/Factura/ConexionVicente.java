package com.mariava.taller.Factura;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase que conecta para devolver el informe
 */
public class ConexionVicente {

    // Atributos para conectar con sql
    public static final String USER = "root";
    public static final String PASSWORD = "";

    /**
     * Metodo para conectar con la base de datos
     * @return
     */
    public static Connection getMySQLConnection() {

        try {
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/taller2", USER, PASSWORD);
            return conn;
        }catch(SQLException e) {
            Logger.getLogger(ConexionVicente.class.getName()).log(Level.SEVERE, null, e);

        }
        return null;
    }
}

