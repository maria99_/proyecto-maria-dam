package com.mariava.taller.Factura;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

/**
 * Clase que reporta el archivo necesario y se ve
 */
public class JasperReport {

    /**
     * Metodo que genera la factura y le decimos como quiere
     * que se nos guarde
     */
    public static void informeFactura(){
        JasperPrint informeLleno = ReportGenerator.generarInformeFactura();

        JasperViewer viewer = new JasperViewer(informeLleno);
        viewer.setVisible(true);

        try {
            JasperExportManager.exportReportToPdfFile(informeLleno, "factura_taller_cliente.pdf");
        }catch (JRException e) {
            e.printStackTrace();
        }
    }

}
