package com.mariava.taller.Clases;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
public class Factura {
    private int idFactura;
    private int nofact;
    private Date fecha;
    private double impuesto;
    private int rfc;
    private String nombreCliente;
    private String dni;
    private int total;
    private Cliente[] cliente;
    private Mecanico[] mecanico;
    private Vehiculo[] vehiculo;
    private Hojap[] hojap;

    private int idCliente;
    private int idHoja;
    private int idMec;
    private int idVehiculo;

    @Id
    @Column(name = "IdFactura")
    public int getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(int idFactura) {
        this.idFactura = idFactura;
    }

    @Basic
    @Column(name = "Nofact")
    public int getNofact() {
        return nofact;
    }

    public void setNofact(int nofact) {
        this.nofact = nofact;
    }

    @Basic
    @Column(name = "Fecha")
    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Basic
    @Column(name = "impuesto")
    public double getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(double impuesto) {
        this.impuesto = impuesto;
    }

    @Basic
    @Column(name = "RFC")
    public int getRfc() {
        return rfc;
    }

    public void setRfc(int rfc) {
        this.rfc = rfc;
    }

    @Basic
    @Column(name = "NombreCliente")
    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    @Basic
    @Column(name = "DNI")
    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    @Basic
    @Column(name = "Total")
    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    @Basic
    @Column(name = "idCliente")
    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    @Basic
    @Column(name = "idHoja")
    public int getIdHoja() {
        return idHoja;
    }

    public void setIdHoja(int idHoja) {
        this.idHoja = idHoja;
    }

    @Basic
    @Column(name = "idMec")
    public int getIdMec() {
        return idMec;
    }

    public void setIdMec(int idMec) {
        this.idMec = idMec;
    }

    @Basic
    @Column(name = "idVehiculo")
    public int getIdVehiculo() {
        return idVehiculo;
    }

    public void setIdVehiculo(int idVehiculo) {
        this.idVehiculo = idHoja;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Factura factura = (Factura) o;
        return idFactura == factura.idFactura &&
                nofact == factura.nofact &&
                Double.compare(factura.impuesto, impuesto) == 0 &&
                rfc == factura.rfc &&
                total == factura.total &&
                Objects.equals(fecha, factura.fecha) &&
                Objects.equals(nombreCliente, factura.nombreCliente) &&
                Objects.equals(dni, factura.dni);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idFactura, nofact, fecha, impuesto, rfc, nombreCliente, dni, total);
    }



    @Override
    public String toString() {
        return  dni + " - " + nombreCliente;
    }
}
