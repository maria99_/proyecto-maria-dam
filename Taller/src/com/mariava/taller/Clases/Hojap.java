package com.mariava.taller.Clases;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
public class Hojap {
    private int idhoja;
    private String concepto;
    private int cantidad;
    private String solicitante;
    private Date fechaPrevistaEntrega;
    private String nombreProveedor;
    private Factura factura;
    private Repuesto repuesto;
    private int IdFac;
    private int Idmec;
    private int Reparacion;


    @Id
    @Column(name = "Idhoja")
    public int getIdhoja() {
        return idhoja;
    }

    public void setIdhoja(int idhoja) {
        this.idhoja = idhoja;
    }

    @Basic
    @Column(name = "concepto")
    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    @Basic
    @Column(name = "Cantidad")
    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    @Basic
    @Column(name = "Solicitante")
    public String getSolicitante() {
        return solicitante;
    }

    public void setSolicitante(String solicitante) {
        this.solicitante = solicitante;
    }

    @Basic
    @Column(name = "FechaPrevistaEntrega")
    public Date getFechaPrevistaEntrega() {
        return fechaPrevistaEntrega;
    }

    public void setFechaPrevistaEntrega(Date fechaPrevistaEntrega) {
        this.fechaPrevistaEntrega = fechaPrevistaEntrega;
    }

    @Basic
    @Column(name = "NombreProveedor")
    public String getNombreProveedor() {
        return nombreProveedor;
    }

    public void setNombreProveedor(String nombreProveedor) {
        this.nombreProveedor = nombreProveedor;
    }

    @Basic
    @Column(name = "IdFac")
    public int getIdFac() {
        return IdFac;
    }

    public void setIdFac(int IdHojap) {
        this.IdFac = IdFac;
    }

    @Basic
    @Column(name = "Idmec")
    public int getIdmec() {
        return Idmec;
    }

    public void setIdmec(int Idmec) {
        this.Idmec = Idmec;
    }

    @Basic
    @Column(name = "Reparacion")
    public int getReparacion() {
        return Reparacion;
    }

    public void setReparacion(int Reparacion) {
        this.Reparacion = Reparacion;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hojap hojap = (Hojap) o;
        return idhoja == hojap.idhoja &&
                cantidad == hojap.cantidad &&
                Objects.equals(concepto, hojap.concepto) &&
                Objects.equals(solicitante, hojap.solicitante) &&
                Objects.equals(fechaPrevistaEntrega, hojap.fechaPrevistaEntrega) &&
                Objects.equals(nombreProveedor, hojap.nombreProveedor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idhoja, concepto, cantidad, solicitante, fechaPrevistaEntrega, nombreProveedor);
    }



    @OneToOne
    @JoinColumn(name = "Idhoja", referencedColumnName = "IdHojap", nullable = false)
    public Repuesto getRepuesto() {
        return repuesto;
    }

    public void setRepuesto(Repuesto repuesto) {
        this.repuesto = repuesto;
    }

    @Override
    public String toString() {
        return concepto;
    }
}
