package com.mariava.taller.Clases;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Repuesto {
    private int idrep;
    private String descripcion;
    private double costoUnico;
    private double precioUnico;
    private String nombrePieza;
    private int cantidadUsada;
    private String solicitante;
    private String proveedor;
    private Hojap hojap;
    private int IdHojap;

    @Id
    @Column(name = "Idrep")
    public int getIdrep() {
        return idrep;
    }

    public void setIdrep(int idrep) {
        this.idrep = idrep;
    }

    @Basic
    @Column(name = "Descripción")
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Basic
    @Column(name = "CostoUnico")
    public double getCostoUnico() {
        return costoUnico;
    }

    public void setCostoUnico(double costoUnico) {
        this.costoUnico = costoUnico;
    }

    @Basic
    @Column(name = "PrecioUnico")
    public double getPrecioUnico() {
        return precioUnico;
    }

    public void setPrecioUnico(double precioUnico) {
        this.precioUnico = precioUnico;
    }

    @Basic
    @Column(name = "NombrePieza")
    public String getNombrePieza() {
        return nombrePieza;
    }

    public void setNombrePieza(String nombrePieza) {
        this.nombrePieza = nombrePieza;
    }

    @Basic
    @Column(name = "CantidadUsada")
    public int getCantidadUsada() {
        return cantidadUsada;
    }

    public void setCantidadUsada(int cantidadUsada) {
        this.cantidadUsada = cantidadUsada;
    }

    @Basic
    @Column(name = "Solicitante")
    public String getSolicitante() {
        return solicitante;
    }

    public void setSolicitante(String solicitante) {
        this.solicitante = solicitante;
    }

    @Basic
    @Column(name = "Proveedor")
    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    @Basic
    @Column(name = "IdHojap")
    public int getIdHojap() {
        return IdHojap;
    }

    public void setIdHojap(int IdHojap) {
        this.IdHojap = IdHojap;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Repuesto repuesto = (Repuesto) o;
        return idrep == repuesto.idrep &&
                Double.compare(repuesto.costoUnico, costoUnico) == 0 &&
                Double.compare(repuesto.precioUnico, precioUnico) == 0 &&
                cantidadUsada == repuesto.cantidadUsada &&
                Objects.equals(descripcion, repuesto.descripcion) &&
                Objects.equals(nombrePieza, repuesto.nombrePieza) &&
                Objects.equals(solicitante, repuesto.solicitante) &&
                Objects.equals(proveedor, repuesto.proveedor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idrep, descripcion, costoUnico, precioUnico, nombrePieza, cantidadUsada, solicitante, proveedor);
    }

    @OneToOne(mappedBy = "repuesto")
    public Hojap getHojap() {
        return hojap;
    }

    public void setHojap(Hojap hojap) {
        this.hojap = hojap;
    }

    @Override
    public String toString() {
        return nombrePieza;
    }
}
