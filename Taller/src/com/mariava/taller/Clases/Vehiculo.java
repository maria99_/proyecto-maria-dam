package com.mariava.taller.Clases;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.util.Objects;

@Entity
public class Vehiculo {
    private int idVehiculo;
    private String matricula;
    private String modelo;
    private String color;
    private Date fechaEnt;
    private Time horaEnt;
    private int kilometros;
    private int RFC;
    private int Mec;
    private Factura factura;

    @Id
    @Column(name = "IdVehiculo")
    public int getIdVehiculo() {
        return idVehiculo;
    }

    public void setIdVehiculo(int idVehiculo) {
        this.idVehiculo = idVehiculo;
    }

    @Basic
    @Column(name = "Matricula")
    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    @Basic
    @Column(name = "Modelo")
    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    @Basic
    @Column(name = "Color")
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Basic
    @Column(name = "Fecha_ent")
    public Date getFechaEnt() {
        return fechaEnt;
    }

    public void setFechaEnt(Date fechaEnt) {
        this.fechaEnt = fechaEnt;
    }

    @Basic
    @Column(name = "Hora_ent")
    public Time getHoraEnt() {
        return horaEnt;
    }

    public void setHoraEnt(Time horaEnt) {
        this.horaEnt = horaEnt;
    }

    @Basic
    @Column(name = "Kilometros")
    public int getKilometros() {
        return kilometros;
    }

    public void setKilometros(int kilometros) {
        this.kilometros = kilometros;
    }

    @Basic
    @Column(name = "RFC")
    public int getRFC() {
        return RFC;
    }

    public void setRFC(int RFC) {
        this.RFC = RFC;
    }

    @Basic
    @Column(name = "Mec")
    public int getMec() {
        return Mec;
    }

    public void setMec(int mec) {
        this.Mec = mec;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vehiculo vehiculo = (Vehiculo) o;
        return idVehiculo == vehiculo.idVehiculo &&
                kilometros == vehiculo.kilometros &&
                Objects.equals(matricula, vehiculo.matricula) &&
                Objects.equals(modelo, vehiculo.modelo) &&
                Objects.equals(color, vehiculo.color) &&
                Objects.equals(fechaEnt, vehiculo.fechaEnt) &&
                Objects.equals(horaEnt, vehiculo.horaEnt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idVehiculo, matricula, modelo, color, fechaEnt, horaEnt, kilometros);
    }



    @Override
    public String toString() {
        return "Matricula: " + matricula;
    }
}
