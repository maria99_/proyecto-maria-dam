package com.mariava.taller.Clases;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Mecanico {
    private int idmec;
    private String nombre;
    private String apellido;
    private String direccion;
    private String tel;
    private double costoxhora;
    private String matriculaCoche;
    private int horasTrabajadas;
    private Factura factura;
    private int IdFac;

    @Id
    @Column(name = "Idmec")
    public int getIdmec() {
        return idmec;
    }

    public void setIdmec(int idmec) {
        this.idmec = idmec;
    }

    @Basic
    @Column(name = "Nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "Apellido")
    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    @Basic
    @Column(name = "Direccion")
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Basic
    @Column(name = "tel")
    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    @Basic
    @Column(name = "Costoxhora")
    public double getCostoxhora() {
        return costoxhora;
    }

    public void setCostoxhora(double costoxhora) {
        this.costoxhora = costoxhora;
    }

    @Basic
    @Column(name = "MatriculaCoche")
    public String getMatriculaCoche() {
        return matriculaCoche;
    }

    public void setMatriculaCoche(String matriculaCoche) {
        this.matriculaCoche = matriculaCoche;
    }

    @Basic
    @Column(name = "HorasTrabajadas")
    public int getHorasTrabajadas() {
        return horasTrabajadas;
    }

    public void setHorasTrabajadas(int horasTrabajadas) {
        this.horasTrabajadas = horasTrabajadas;
    }

    @Basic
    @Column(name = "IdFac")
    public int getIdFac() {
        return IdFac;
    }

    public void setIdFac(int idFac) {
        this.IdFac = idFac;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Mecanico mecanico = (Mecanico) o;
        return idmec == mecanico.idmec &&
                Double.compare(mecanico.costoxhora, costoxhora) == 0 &&
                horasTrabajadas == mecanico.horasTrabajadas &&
                Objects.equals(nombre, mecanico.nombre) &&
                Objects.equals(apellido, mecanico.apellido) &&
                Objects.equals(direccion, mecanico.direccion) &&
                Objects.equals(tel, mecanico.tel) &&
                Objects.equals(matriculaCoche, mecanico.matriculaCoche);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idmec, nombre, apellido, direccion, tel, costoxhora, matriculaCoche, horasTrabajadas);
    }




    @Override
    public String toString() {
        return  nombre + " " + apellido;
    }
}
