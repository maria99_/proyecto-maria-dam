package com.mariava.taller.gui;

import com.mariava.taller.Clases.*;
import com.mariava.taller.Factura.JasperReport;
import com.mariava.taller.Vista.VistaVentana;
import com.mariava.taller.VistaUsuarios.VistaMecanico;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase que genera todos los metodos y todos los botones de la aplicacion
 */
public class Controlador implements ActionListener, ListSelectionListener, FocusListener, ChangeListener {

    //Atributos
    private VistaVentana vista;
    private Modelo modelo;

    /**
     * Generador de un constructor
     *
     * @param vista  variable llamada asi, referente a la clase vista
     * @param modelo variable llamada asi, referente a la clase modelo
     */
    public Controlador(VistaVentana vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        addFocusListeners(this);
        addActionListeners(this);
        addListSelectionListener(this);
        //addChangeListeners(this);
    }

    /**
     * Añado listener para que al pulsar sobre cada pestaña se carguen los datos referentes
     *
     * @param controlador variable llamada asi
     */
    private void addChangeListeners(ChangeListener controlador) {
        vista.tabbedPane1.addChangeListener(controlador);

    }

    /**
     * Añado los listener de todas las listas sobre las que puedo seleccionar elementos
     *
     * @param controlador variable llamada asi
     */
    private void addListSelectionListener(Controlador controlador) {
        vista.listarClientes.addListSelectionListener(controlador);
        vista.listarMecanico.addListSelectionListener(controlador);
        vista.listarVehiculos.addListSelectionListener(controlador);
        vista.listarFactura.addListSelectionListener(controlador);
        vista.listarRepuesto.addListSelectionListener(controlador);
        vista.listarHoja.addListSelectionListener(controlador);
    }

    /**
     * Añado los listener de todos los botones
     *
     * @param listener variable llamada asi
     */
    private void addActionListeners(Controlador listener) {
        //Menu
        vista.salirItem.addActionListener(listener);

        // Cliente
        vista.registrarClienteButton.addActionListener(listener);
        vista.modificarClienteButton.addActionListener(listener);
        vista.eliminarClienteButton.addActionListener(listener);
        vista.listarClienteButton.addActionListener(listener);

        //Mecanico
        vista.registrarMecanicoButton.addActionListener(listener);
        vista.modificarMecanicoButton.addActionListener(listener);
        vista.eliminarMecanicoButton.addActionListener(listener);
        vista.listarMecanicoButton.addActionListener(listener);

        // Vehiculo
        vista.registrarVehiculoButton.addActionListener(listener);
        vista.modificarVehiculoButton.addActionListener(listener);
        vista.eliminarVehiculoButton.addActionListener(listener);
        vista.listarVehiculoButton.addActionListener(listener);

        //Factura
        vista.modificarFacturaButton.addActionListener(listener);
        vista.eliminarFacturaButton.addActionListener(listener);
        vista.listarFacturaButton.addActionListener(listener);
        vista.generarFacturaButton.addActionListener(listener);

        //Repuesto
        vista.registrarRepuestoButton.addActionListener(listener);
        vista.modificarRepuestoButton.addActionListener(listener);
        vista.eliminarRepuestoButton.addActionListener(listener);
        vista.listarRepuestoButton.addActionListener(listener);
        vista.graficoPiezasButton.addActionListener(listener);

        //Hoja pedido
        vista.modificarHojaButton.addActionListener(listener);
        vista.eliminarHojaButton.addActionListener(listener);
        vista.listarHojaButton.addActionListener(listener);

    }

    /**
     * Añado los listeners de de foco en los comboboxes de la seccion Hoja pedido
     *
     * @param controlador variable llamada asi
     */
    private void addFocusListeners(FocusListener controlador) {
        vista.comboBoxPiezaHoja.addFocusListener(controlador);
    }


    /**
     * Metodos listeners de accion, seleccion, clic de raton, y foco
     * Listener de los eventos de boton
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();

         if (vista.tabbedPane1.getSelectedComponent() == vista.panelClientes) {
             switch (comando) {
                 //Menu de opciones
                 case "Salir":
                     modelo.desconectar();
                     System.exit(0);
                     break;

                 //Clientes botones
                 case "RegistrarClientes":
                     nuevoCliente();
                     vista.txtEstado.setText("Estado: Cliente añadido");
                     break;
                 case "ModificarClientes":
                     modificarCliente();
                     vista.txtEstado.setText("Estado: Cliente Modificado");
                     break;
                 case "EliminarClientes":
                     eliminarCliente();
                     vista.txtEstado.setText("Estado: Cliente eliminado");
                     break;
                 case "ListarClientes":
                     listarClientes();
                     vista.txtEstado.setText("Estado: Listado de clientes");
                     break;
             }
        } else if (vista.tabbedPane1.getSelectedComponent() == vista.panelMecanicos) {
            switch (comando) {
                case "Salir":
                    modelo.desconectar();
                    System.exit(0);
                    break;

                //Mecanicos Botones
                case "RegistrarMecanico":
                    nuevoMecanico();
                    vista.txtEstado.setText("Estado: Mecanico añadido");
                    break;
                case "ModificarMecanico":
                    modificarMecanico();
                    vista.txtEstado.setText("Estado: Mecanico modificado");
                    break;
                case "EliminarMecanico":
                    eliminarMecanico();
                    vista.txtEstado.setText("Estado: Mecanico eliminado");
                    break;
                case "ListarMecanicos":
                    listarMecanico();
                    vista.txtEstado.setText("Estado: Listado de mecanicos");
                    break;
            }
        } else if (vista.tabbedPane1.getSelectedComponent() == vista.panelVehiculos) {
            switch (comando) {
                case "Salir":
                    modelo.desconectar();
                    System.exit(0);
                    break;

                //Coches botones
                case "RegistrarVehiculo":
                    nuevoVehiculo();
                    vista.txtEstado.setText("Estado: Vehiculo añadido");
                    break;
                case "ModificarVehiculo":
                    modificarVehiculo();
                    vista.txtEstado.setText("Estado: Vehiculo modificado");
                    break;
                case "EliminarVehiculo":
                    eliminarVehiculo();
                    vista.txtEstado.setText("Estado: Vehiculo eliminado");
                    break;
                case "ListarVehiculo":
                    listarVehiculos();
                    vista.txtEstado.setText("Estado: Listado de vehiculos");
                    break;
            }
        } else if (vista.tabbedPane1.getSelectedComponent() == vista.panelFactura) {
            switch (comando) {
                case "Salir":
                    modelo.desconectar();
                    System.exit(0);
                    break;
                //Factura botones
                case "RegistrarFactura":
                    nuevaFactura();
                    vista.txtEstado.setText("Estado: Factura añadido");
                    break;
                case "ModificarFactura":
                    modificarFactura();
                    vista.txtEstado.setText("Estado: Factura modificado");
                    break;
                case "EliminarFactura":
                    eliminarFactura();
                    vista.txtEstado.setText("Estado: Factura eliminado");
                    break;
                case "ListarFactura":
                    listarFactura();
                    vista.txtEstado.setText("Estado: Listado de factura");
                    break;
                case "Factura":
                    JasperReport.informeFactura();
                    vista.txtEstado.setText("Estado: Generando factura de cliente");
                    break;
            }
        } else if (vista.tabbedPane1.getSelectedComponent() == vista.panelRepuesto) {
            switch (comando) {
                case "Salir":
                    modelo.desconectar();
                    System.exit(0);
                    break;

                //Repuesto botones
                case "RegistrarRepuesto":
                    nuevoRepuesto();
                    vista.txtEstado.setText("Estado: Repuesto añadido");
                    break;
                case "ModificarRepuesto":
                    modificarRepuesto();
                    vista.txtEstado.setText("Estado: Repuesto modificado");
                    break;
                case "EliminarRepuesto":
                    eliminarRepuesto();
                    vista.txtEstado.setText("Estado: Repuesto eliminado");
                    break;
                case "ListarRepuesto":
                    listarRepuesto();
                    vista.txtEstado.setText("Estado: Listado de Repuesto");
                    break;
                case "MostrarGrafico":
                    mostrarGraficoPiezas();
                    break;
            }
        } else if (vista.tabbedPane1.getSelectedComponent() == vista.panelHoja) {
            switch (comando) {
                case "Salir":
                    modelo.desconectar();
                    System.exit(0);
                    break;

                case "RegistrarHoja":
                    nuevaHoja();
                    vista.txtEstado.setText("Estado: Hoja de pedido añadida");
                    break;
                case "ModificarHoja":
                    modificarHoja();
                    vista.txtEstado.setText("Estado: Hoja de pedido modificada");
                    break;
                case "EliminarHoja":
                    eliminarHoja();
                    vista.txtEstado.setText("Estado: Hoja de pedido eliminada");
                    break;
                case "ListarHoja":
                    listarHojaPedido();
                    vista.txtEstado.setText("Estado: Listado de hoja de pedido");
                    break;
            }
        }

    }

    //Metodos hoja pedido

    /**
     * Cargos las hojas de pedido en la lista de la seccion hoja de pedidos
     */
    private void listarHojaPedido() {
        ArrayList<Hojap> lista = modelo.getHoja();
        vista.dlmHoja.clear();
        for (Hojap unaHoja : lista) {
            vista.dlmHoja.addElement(unaHoja);
        }
    }

    /**
     * Elimino una hoja de pedido
     */
    private void eliminarHoja() {
        Hojap hojaBorrada = (Hojap) vista.listarHoja.getSelectedValue();
        modelo.borrarHoja(hojaBorrada);
    }

    /**
     * Mpico los datos que quiero de la hoja de pedido
     */
    private void modificarHoja() {
        Hojap hojaSeleccionada = (Hojap) vista.listarHoja.getSelectedValue();
        hojaSeleccionada.setConcepto(vista.txtConceptoHoja.getText());
        modelo.modificarhoja(hojaSeleccionada);
    }

    /**
     * Registro nueva hoja de pedido
     */
    private void nuevaHoja() {
        Hojap nuevaHoja = new Hojap();
        nuevaHoja.setIdhoja(Integer.parseInt(vista.txtIdHoja.getText()));
        nuevaHoja.setIdFac(Integer.parseInt(vista.txtIdFacturaHoja.getText()));
        nuevaHoja.setConcepto(vista.txtConceptoHoja.getText());
        nuevaHoja.setCantidad(Integer.parseInt(vista.txtCantidadHoja.getText()));
        nuevaHoja.setSolicitante(vista.txtSolicitanteHoja.getText());
        nuevaHoja.setFechaPrevistaEntrega(Date.valueOf(vista.datePickerHoja.getDate()));
        nuevaHoja.setNombreProveedor(vista.txtNombreHoja.getText());
        nuevaHoja.setIdmec(Integer.parseInt(vista.txtIdMecHoja.getText()));
        modelo.altaHoja(nuevaHoja);
    }

    /**
     * Limpio los campos de hoja de pedido
     */
    private void limpiarCamposHoja() {
        vista.txtIdHoja.setText("");
        vista.txtConceptoHoja.setText("");
        vista.txtCantidadHoja.setText("");
        vista.txtSolicitanteHoja.setText("");
        vista.datePickerHoja.setText("");
        vista.txtNombreHoja.setText("");
    }

    //Metodos Repuesto

    /**
     * Genero un grafico con las piezas y el nombre de la clase repuesto
     */
    private void mostrarGraficoPiezas() {
        DefaultPieDataset dataset = new DefaultPieDataset();
        for (Repuesto repuesto : modelo.getRepuesto()) {
            if (repuesto != null) {
                dataset.setValue(repuesto.getNombrePieza(), repuesto.getCantidadUsada());
            }

        }
        JFreeChart chart = ChartFactory.createPieChart("Cantidad de piezas", dataset, true, true, true);
        ChartPanel panel = new ChartPanel(chart);
        JDialog dialog = new JDialog();
        dialog.setContentPane(panel);
        dialog.setTitle("Grafico de piezas");
        dialog.setModal(true);
        dialog.setSize(500, 500);
        dialog.setLocationRelativeTo(null);
        dialog.setVisible(true);
    }

    /**
     * Cargo los repuestos en la lista de la seccion repuestos
     */
    private void listarRepuesto() {
        ArrayList<Repuesto> lista = modelo.getRepuesto();
        vista.dlmRepuesto.clear();
        for (Repuesto unRepuesto : lista) {
            vista.dlmRepuesto.addElement(unRepuesto);
        }
    }

    /**
     * elimino los repuestos
     */
    private void eliminarRepuesto() {
        Repuesto repuestoBorrado = (Repuesto) vista.listarRepuesto.getSelectedValue();
        modelo.borrarRepuesto(repuestoBorrado);
    }

    /**
     * Modifico los repuestos
     */
    private void modificarRepuesto() {
        Repuesto repuestoSeleccionado = (Repuesto) vista.listarRepuesto.getSelectedValue();
        repuestoSeleccionado.setNombrePieza(vista.txtNombreRepuesto.getText());
        modelo.modificarRepuesto(repuestoSeleccionado);
    }

    /**
     * Genero nuevos rpuesto
     */
    private void nuevoRepuesto() {
        Repuesto nuevoRepuesto = new Repuesto();
        nuevoRepuesto.setIdrep(Integer.parseInt(vista.txtIdRepuesto.getText()));
        nuevoRepuesto.setDescripcion(vista.txtDescripcionReo.getText());
        nuevoRepuesto.setCostoUnico(Double.parseDouble(vista.txtCostoRepuesto.getText()));
        nuevoRepuesto.setPrecioUnico(Double.parseDouble(vista.txtPrecioRepuesto.getText()));
        nuevoRepuesto.setNombrePieza(vista.txtNombreRepuesto.getText());
        nuevoRepuesto.setCantidadUsada(Integer.parseInt(vista.txtCantidadRepuesto.getText()));
        nuevoRepuesto.setSolicitante(vista.txtSolicitanteRepuesto.getText());
        nuevoRepuesto.setProveedor(vista.txtProveedorRepuesto.getText());
        nuevoRepuesto.setIdHojap(Integer.parseInt(vista.txtidHojaRepuesto.getText()));
        modelo.altaRepuesto(nuevoRepuesto);
    }

    /**
     * Limpio los campos de repuesto
     */
    private void limpiarCamposRepuesto() {
        vista.txtIdRepuesto.setText("");
        vista.txtDescripcionReo.setText("");
        vista.txtCostoRepuesto.setText("");
        vista.txtPrecioRepuesto.setText("");
        vista.txtPrecioRepuesto.setText("");
        vista.txtSolicitanteRepuesto.setText("");
        vista.txtProveedorRepuesto.setText("");
    }

    //Metodos factura

    /**
     * Cargos las facturas en la lista de la seccion facturas
     */
    private void listarFactura() {
        ArrayList<Factura> lista = modelo.getFactura();
        vista.dlmFactura.clear();
        for (Factura unaFactura : lista) {
            vista.dlmFactura.addElement(unaFactura);
        }
    }

    /**
     * Elimino las facturas de la vista mecanico
     */
    private void eliminarFactura() {
        Factura facturaBorrada = (Factura) vista.listarFactura.getSelectedValue();
        modelo.borrarFactura(facturaBorrada);
    }

    /**
     * Modifico las facturas
     */
    private void modificarFactura() {
        Factura facturaSaleccionada = (Factura) vista.listarFactura.getSelectedValue();
        facturaSaleccionada.setNombreCliente(vista.txtNombreFactura.getText());
        facturaSaleccionada.setDni(vista.txtDniFactura.getText());
        modelo.modificarFactura(facturaSaleccionada);
    }

    /**
     * Creo nuevas facturas nuevas
     */
    private void nuevaFactura() {
        Factura nuevaFactura = new Factura();
        nuevaFactura.setIdFactura(Integer.parseInt(vista.txtIdFactura.getText()));
        nuevaFactura.setIdCliente(Integer.parseInt(vista.txtIdClienteFactura.getText()));
        nuevaFactura.setIdHoja(Integer.parseInt(vista.txtIdHojaFactura.getText()));

        nuevaFactura.setIdMec(Integer.parseInt(vista.txtIdMecFactura.getText()));
        nuevaFactura.setIdVehiculo(Integer.parseInt(vista.txtIdVehiculoFactura.getText()));

        nuevaFactura.setNofact(Integer.parseInt(vista.txtNumeroFactura.getText()));
        nuevaFactura.setFecha(Date.valueOf(vista.datePickerFactura.getDate()));
        nuevaFactura.setImpuesto(Integer.parseInt(vista.txtImpuestoFactura.getText()));
        nuevaFactura.setRfc(Integer.parseInt(vista.txtRfcFactura.getText()));
        nuevaFactura.setNombreCliente(vista.txtNombreFactura.getText());
        nuevaFactura.setDni(vista.txtDniFactura.getText());
        nuevaFactura.setTotal(Integer.parseInt(vista.txtTotalFactura.getText()));
        modelo.altaFactura(nuevaFactura);
    }

    /**
     * Limpio los campos de factura
     */
    private void limpiarCamposFactura() {
        vista.txtIdFactura.setText("");
        vista.txtNumeroFactura.setText("");
        vista.datePickerFactura.setText("");
        vista.txtImpuestoFactura.setText("");
        vista.txtRfcFactura.setText("");
        vista.txtNombreFactura.setText("");
        vista.txtDniFactura.setText("");
        vista.txtTotalFactura.setText("");
    }

    //Metodos para vehiculos

    /**
     * Cargos los vehiculos en la lista de la seccion vehiculos
     */
    private void listarVehiculos() {
        ArrayList<Vehiculo> lista = modelo.getVehiculo();
        vista.dlmVehiculo.clear();
        for (Vehiculo unVehiculo : lista) {
            vista.dlmVehiculo.addElement(unVehiculo);
        }
    }

    /**
     * Elimino vehiculos
     */
    private void eliminarVehiculo() {
        Vehiculo vehiculoBorrado = (Vehiculo) vista.listarVehiculos.getSelectedValue();
        modelo.borrarVehiculo(vehiculoBorrado);
    }

    /**
     * Modifico vehiculos
     */
    private void modificarVehiculo() {
        Vehiculo cocheSeleccion = (Vehiculo) vista.listarVehiculos.getSelectedValue();
        cocheSeleccion.setMatricula(vista.txtMatriculaVehiculo.getText());
        modelo.modificarVehiculo(cocheSeleccion);
    }


    /**
     * Creo nuevos vehiculos
     */
    private void nuevoVehiculo() {
        Vehiculo nuevoVehiculo = new Vehiculo();
        nuevoVehiculo.setIdVehiculo(Integer.parseInt(vista.txtIdVehiculo.getText()));
        nuevoVehiculo.setMatricula(vista.txtMatriculaVehiculo.getText());
        nuevoVehiculo.setModelo(vista.txtModeloVehiculo.getText());
        nuevoVehiculo.setColor(vista.txtColorVehiculo.getText());
        nuevoVehiculo.setFechaEnt(Date.valueOf(vista.datePicker.getDate()));
        nuevoVehiculo.setHoraEnt(Time.valueOf(vista.timePicker.getTime()));
        nuevoVehiculo.setKilometros(Integer.parseInt(vista.txtKmVehiculo.getText()));
        nuevoVehiculo.setRFC(Integer.parseInt(vista.txtRfcVehiculo.getText()));
        nuevoVehiculo.setMec(Integer.parseInt(vista.txtMecVehiculo.getText()));
        modelo.altaVehiculo(nuevoVehiculo);
    }

    /**
     * Limpio los campos de vehiculo
     */
    private void limpiarCamposVehiculo() {
        vista.txtIdVehiculo.setText("");
        vista.txtMatriculaVehiculo.setText("");
        vista.txtModeloVehiculo.setText("");
        vista.txtColorVehiculo.setText("");
        vista.datePicker.setText("");
        vista.timePicker.setText("");
        vista.txtKmVehiculo.setText("");
    }

    //Metodos Mecanico

    /**
     * Cargos los mecanicos en la lista de la seccion mecanicos
     */
    private void listarMecanico() {
        ArrayList<Mecanico> lista = modelo.getMecanico();
        vista.dlmMecanico.clear();
        for (Mecanico unMecanico : lista) {
            vista.dlmMecanico.addElement(unMecanico);
        }
    }

    /**
     * elimino los mecanicos
     */
    private void eliminarMecanico() {
        Mecanico mecanicoBorrado = (Mecanico) vista.listarMecanico.getSelectedValue();
        modelo.borrarMecanico(mecanicoBorrado);
    }

    /**
     * Modifico los mecanicos
     */
    private void modificarMecanico() {
        Mecanico mecanicoSeleccion = (Mecanico) vista.listarMecanico.getSelectedValue();
        mecanicoSeleccion.setApellido(vista.txtApellidoMecanico.getText());
        modelo.modificarMecanico(mecanicoSeleccion);
    }

    /**
     * Genero nuevos mecanicos
     */
    private void nuevoMecanico() {
        Mecanico nuevoMecanico = new Mecanico();
        nuevoMecanico.setIdmec(Integer.parseInt(vista.txtIdMecanico.getText()));
        nuevoMecanico.setIdFac(Integer.parseInt(vista.txtIdFacMecanico.getText()));
        nuevoMecanico.setNombre(vista.txtNombreMecanico.getText());
        nuevoMecanico.setApellido(vista.txtApellidoMecanico.getText());
        nuevoMecanico.setDireccion(vista.txtDireccionMecanico.getText());
        nuevoMecanico.setTel(vista.txtTelefonoMecanico.getText());
        nuevoMecanico.setCostoxhora(Integer.parseInt(vista.txtCostoMecanico.getText()));
        nuevoMecanico.setMatriculaCoche(vista.txtMatriculaMecanico.getText());
        modelo.altaMecanico(nuevoMecanico);
    }

    /**
     * Limpio los campos de mecanicos
     */
    private void limpiarCamposMecanico() {
        vista.txtIdMecanico.setText("");
        vista.txtNombreMecanico.setText("");
        vista.txtApellidoMecanico.setText("");
        vista.txtDireccionMecanico.setText("");
        vista.txtTelefonoMecanico.setText("");
        vista.txtCostoMecanico.setText("");
        vista.txtHorasMecanico.setText("");
    }

    //Metodos Clientes

    /**
     * Cargos los clientes en la lista de la seccion clientes
     */
    private void listarClientes() {
        ArrayList<Cliente> lista = modelo.getCliente();
        vista.dlmClientes.clear();
        for (Cliente unCliente : lista) {
            vista.dlmClientes.addElement(unCliente);
        }
    }

    /**
     * Elimino los clientes
     */
    private void eliminarCliente() {
        Cliente clienteBorrado = (Cliente) vista.listarClientes.getSelectedValue();
        modelo.borrarCliente(clienteBorrado);
    }

    /**
     * Modifico los clientes
     */
    private void modificarCliente() {
        Cliente clienteSeleccion = (Cliente) vista.listarClientes.getSelectedValue();
        clienteSeleccion.setNombre(vista.txtNombreCliente.getText());
        clienteSeleccion.setDni(vista.txtDNICliente.getText());
        modelo.modificarCliente(clienteSeleccion);
    }

    /**
     * Creo nuevos clientes
     */
    private void nuevoCliente() {
        Cliente nuevoCliente = new Cliente();
        nuevoCliente.setIdCliente(Integer.parseInt(vista.txtIdCliente.getText()));
        nuevoCliente.setNombre(vista.txtNombreCliente.getText());
        nuevoCliente.setRFC(Integer.parseInt(vista.txtRfcCliente.getText()));

        nuevoCliente.setDireccion(vista.txtDireccionCliente.getText());
        nuevoCliente.setTel(vista.txtTelefonoCliente.getText());
        nuevoCliente.setCp(Integer.parseInt(vista.txtCodigoPostal.getText()));
        nuevoCliente.setPoblacion(vista.txtPoblacionCliente.getText());
        nuevoCliente.setDni(vista.txtDNICliente.getText());
        nuevoCliente.setEmail(vista.txtEmailCliente.getText());
        modelo.altaCliente(nuevoCliente);
    }

    /**
     * Limpio los campos de cliente
     */
    private void limpiarCamposCliente() {
        vista.txtIdCliente.setText("");
        vista.txtNombreCliente.setText("");
        vista.txtDireccionCliente.setText("");
        vista.txtTelefonoCliente.setText("");
        vista.txtCodigoPostal.setText("");
        vista.txtPoblacionCliente.setText("");
        vista.txtDNICliente.setText("");
        vista.txtEmailCliente.setText("");
    }

    /**
     * Se ejecuta cuando un combobox gana el foco (seleccionamos un combo)
     * carga las piezas en el comboBox que recibe el foco
     *
     * @param e
     */
    @Override
    public void focusGained(FocusEvent e) {
        if (e.getSource() == vista.comboBoxPiezaHoja) {
            vista.comboBoxPiezaHoja.hidePopup();
            listarPiezaHoja();
            vista.comboBoxPiezaHoja.showPopup();
        }
    }

    /**
     * Relleno el combobox con piezas
     */
    private void listarPiezaHoja() {
        List<Hojap> listaHoja = modelo.getHoja();
        vista.dcbmHoja.removeAllElements();
        for (Hojap hoja : listaHoja) {
            vista.dcbmHoja.addElement(hoja);
        }
    }

    /**
     * Actualizo los campos necesarios
     *
     * @param e
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
            if (e.getSource() == vista.listCliente) {
                Cliente clienteSeleccion = (Cliente) vista.listarClientes.getSelectedValue();
                vista.txtIdCliente.setText(String.valueOf(clienteSeleccion.getIdCliente()));
                vista.txtNombreCliente.setText(clienteSeleccion.getNombre());
                vista.txtRfcCliente.setText(String.valueOf(clienteSeleccion.getRFC()));
                vista.txtDireccionCliente.setText(clienteSeleccion.getDireccion());
                vista.txtTelefonoCliente.setText(clienteSeleccion.getTel());
                vista.txtCodigoPostal.setText(String.valueOf(clienteSeleccion.getCp()));
                vista.txtPoblacionCliente.setText(clienteSeleccion.getPoblacion());
                vista.txtDNICliente.setText(clienteSeleccion.getDni());
                vista.txtEmailCliente.setText(clienteSeleccion.getEmail());
            }
            if (e.getSource() == vista.listMecanico) {
                Mecanico mecanicoSeleccionado = (Mecanico) vista.listarMecanico.getSelectedValue();
                vista.txtIdMecanico.setText(String.valueOf(mecanicoSeleccionado.getIdmec()));
                vista.txtIdFacMecanico.setText(String.valueOf(mecanicoSeleccionado.getIdFac()));
                vista.txtNombreMecanico.setText(mecanicoSeleccionado.getNombre());
                vista.txtApellidoMecanico.setText(mecanicoSeleccionado.getApellido());
                vista.txtDireccionMecanico.setText(mecanicoSeleccionado.getDireccion());
                vista.txtTelefonoMecanico.setText(mecanicoSeleccionado.getTel());
                vista.txtCostoMecanico.setText(String.valueOf(mecanicoSeleccionado.getCostoxhora()));
                vista.txtHorasMecanico.setText(String.valueOf(mecanicoSeleccionado.getHorasTrabajadas()));
            }
            if (e.getSource() == vista.listVehiculo) {
                Vehiculo vehiculoSeleccionado = (Vehiculo) vista.listarVehiculos.getSelectedValue();
                vista.txtIdVehiculo.setText(String.valueOf(vehiculoSeleccionado.getIdVehiculo()));
                vista.txtMatriculaVehiculo.setText(vehiculoSeleccionado.getMatricula());
                vista.txtModeloVehiculo.setText(vehiculoSeleccionado.getModelo());
                vista.txtColorVehiculo.setText(vehiculoSeleccionado.getColor());
                vista.datePicker.setText(String.valueOf(vehiculoSeleccionado.getFechaEnt()));
                vista.timePicker.setText(String.valueOf(vehiculoSeleccionado.getHoraEnt()));
                vista.txtKmVehiculo.setText(String.valueOf(vehiculoSeleccionado.getKilometros()));
                vista.txtRfcVehiculo.setText(String.valueOf(vehiculoSeleccionado.getRFC()));
                vista.txtMecVehiculo.setText(String.valueOf(vehiculoSeleccionado.getMec()));
            }
            if (e.getSource() == vista.listFactura) {
                Factura facturaSeleccionada = (Factura) vista.listarFactura.getSelectedValue();
                vista.txtIdFactura.setText(String.valueOf(facturaSeleccionada.getIdFactura()));
                vista.txtIdClienteFactura.setText(String.valueOf(facturaSeleccionada.getIdCliente()));
                vista.txtIdHojaFactura.setText(String.valueOf(facturaSeleccionada.getIdHoja()));
                vista.txtIdMecFactura.setText(String.valueOf(facturaSeleccionada.getIdMec()));
                vista.txtIdVehiculoFactura.setText(String.valueOf(facturaSeleccionada.getIdVehiculo()));

                vista.txtNumeroFactura.setText(String.valueOf(facturaSeleccionada.getNofact()));
                vista.datePickerFactura.setText(String.valueOf(facturaSeleccionada.getFecha()));
                vista.txtImpuestoFactura.setText(String.valueOf(facturaSeleccionada.getImpuesto()));
                vista.txtRfcFactura.setText(String.valueOf(facturaSeleccionada.getRfc()));
                vista.txtNombreFactura.setText(facturaSeleccionada.getNombreCliente());
                vista.txtDniFactura.setText(facturaSeleccionada.getDni());
                vista.txtTotalFactura.setText(String.valueOf(facturaSeleccionada.getTotal()));
            }
            if (e.getSource() == vista.listRepuesto) {
                Repuesto repuestoSeleccionado = (Repuesto) vista.listarRepuesto.getSelectedValue();
                vista.txtIdRepuesto.setText(String.valueOf(repuestoSeleccionado.getIdrep()));
                vista.txtDescripcionReo.setText(repuestoSeleccionado.getDescripcion());
                vista.txtCostoRepuesto.setText(String.valueOf(repuestoSeleccionado.getCostoUnico()));
                vista.txtPrecioRepuesto.setText(String.valueOf(repuestoSeleccionado.getPrecioUnico()));
                vista.txtNombreRepuesto.setText(repuestoSeleccionado.getNombrePieza());
                vista.txtCantidadRepuesto.setText(String.valueOf(repuestoSeleccionado.getCantidadUsada()));
                vista.txtSolicitanteRepuesto.setText(repuestoSeleccionado.getSolicitante());
                vista.txtProveedorRepuesto.setText(repuestoSeleccionado.getProveedor());
                vista.txtidHojaRepuesto.setText(String.valueOf(repuestoSeleccionado.getIdHojap()));
            }
            if (e.getSource() == vista.listHoja) {
                Hojap hojaSeleccionada = (Hojap) vista.listarHoja.getSelectedValue();
                vista.txtIdHoja.setText(String.valueOf(hojaSeleccionada.getIdhoja()));
                vista.txtIdFacturaHoja.setText(String.valueOf(hojaSeleccionada.getIdFac()));
                vista.txtConceptoHoja.setText(hojaSeleccionada.getConcepto());
                vista.txtCantidadHoja.setText(String.valueOf(hojaSeleccionada.getCantidad()));
                vista.txtSolicitanteHoja.setText(hojaSeleccionada.getSolicitante());
                vista.datePickerHoja.setText(String.valueOf(hojaSeleccionada.getFechaPrevistaEntrega()));
                vista.comboBoxPiezaHoja.setSelectedItem(hojaSeleccionada.getRepuesto());
                vista.txtNombreHoja.setText(hojaSeleccionada.getNombreProveedor());
                vista.txtRepHoja.setText(String.valueOf(hojaSeleccionada.getReparacion()));
                vista.txtIdMecHoja.setText(String.valueOf(hojaSeleccionada.getIdmec()));
            }
        }
    }
    /**
     *  Si selecciono alguna otra pestaña cargo sus datos
     *  La pestaña clientes se carga por defecto al inicializar
     * @param e
     */
    public void stateChanged(ChangeEvent e) {
        int panelSeleccionado = vista.tabbedPane1.getSelectedIndex();
        switch (panelSeleccionado) {
            case 0:
                limpiarCamposCliente();
                break;
            case 1:
                limpiarCamposMecanico();
                break;
            case 2:
                limpiarCamposVehiculo();
                break;
            case 3:
                limpiarCamposFactura();
                break;
            case 4:
                limpiarCamposRepuesto();
                break;
            case 5:
                limpiarCamposHoja();
                break;

        }
    }

    //Sin emplear pero necesario implementarlo
    @Override
    public void focusLost(FocusEvent e) {

    }
}
