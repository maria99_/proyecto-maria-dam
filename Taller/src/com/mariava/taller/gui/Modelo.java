package com.mariava.taller.gui;

import com.mariava.taller.Clases.*;
import com.mariava.taller.Util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;
import java.util.ArrayList;

/**
 * Clase con todas las operaciones de hibernate
 */
public class Modelo {

    // Atributo de sesion
    SessionFactory sessionFactory;

    /**
     * Metodo que desconecta la sesion de hibernnate
     */
    public void desconectar() {
        if(sessionFactory != null && sessionFactory.isOpen())
            sessionFactory.close();
    }

    /**
     * Metodo que conecta con la sesion de hibernate
     */
    public void conectar() {
        Configuration configuracion = new Configuration();
        configuracion.configure("hibernate.cfg.xml");

        configuracion.addAnnotatedClass(Cliente.class);
        configuracion.addAnnotatedClass(Factura.class);
        configuracion.addAnnotatedClass(Hojap.class);
        configuracion.addAnnotatedClass(Mecanico.class);
        configuracion.addAnnotatedClass(Repuesto.class);
        configuracion.addAnnotatedClass(Vehiculo.class);

        StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().applySettings(
                configuracion.getProperties()).build();

        sessionFactory = configuracion.buildSessionFactory(ssr);

    }

    //Metodos de Cliente

    /**
     * Metodo que da de alta a un cliente
     * @param nuevoCliente variable llamada asi
     */
    public void altaCliente(Cliente nuevoCliente) {
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoCliente);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Metodo que lista el cliente
     * @return
     */
    public ArrayList<Cliente> getCliente() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Cliente ");
        ArrayList<Cliente> lista = (ArrayList<Cliente>)query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * metodo que modificar aaal cliente
     * @param clienteSeleccionado variable llamada asi
     */
    public void modificarCliente(Cliente clienteSeleccionado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(clienteSeleccionado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo que borra al cliente
     * @param clienteBorrado varibale llamada asi
     */
    public void borrarCliente(Cliente clienteBorrado) {
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(clienteBorrado);
        sesion.getTransaction().commit();
    }

    //Metodos de Mecanico

    /**
     * Metodo que da de alta a un mecanico
     * @param nuevoMecanico variable llamada asi
     */
    public void altaMecanico(Mecanico nuevoMecanico) {
        //Obtengo una session a partir de la factoria de sesiones
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoMecanico);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Metodo que lista a los mecanicos
     * @return
     */
    public ArrayList<Mecanico> getMecanico() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Mecanico ");
        ArrayList<Mecanico> lista = (ArrayList<Mecanico>)query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * Metodo que modifica a los mecanicos
     * @param mecanicoSeleccionado variable llamada asi
     */
    public void modificarMecanico(Mecanico mecanicoSeleccionado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(mecanicoSeleccionado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo que borrar al mecanico
     * @param mecanicoBorrado variable llamada asi
     */
    public void borrarMecanico(Mecanico mecanicoBorrado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(mecanicoBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    //Metodos de Vehiculo

    /**
     * Metodo que da de alta a un vehiculo
     * @param nuevoVehiculo variable llamada asi
     */
    public void altaVehiculo(Vehiculo nuevoVehiculo) {
        //Obtengo una session a partir de la factoria de sesiones
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoVehiculo);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Metodoq que lista un vehiculo
     * @return
     */
    public ArrayList<Vehiculo> getVehiculo() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Vehiculo");
        ArrayList<Vehiculo> lista = (ArrayList<Vehiculo>)query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * Metodo que modifica un vehiculo
     * @param vehiculoSeleccionado variable llamada asi
     */
    public void modificarVehiculo(Vehiculo vehiculoSeleccionado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(vehiculoSeleccionado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo que borra un vehiculo
     * @param vehiculoBorrado variable llamada asi
     */
    public void borrarVehiculo(Vehiculo vehiculoBorrado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(vehiculoBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    //Metodo de Facturas

    /**
     * Metood que da de alta una factura
     * @param nuevaFactura variable llamada asi
     */
    public void altaFactura(Factura nuevaFactura) {
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevaFactura);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Metodo que lista una factura
     * @return
     */
    public ArrayList<Factura> getFactura() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Factura ");
        ArrayList<Factura> lista = (ArrayList<Factura>)query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * Metodo que modifica una factura
     * @param facturaSeleccionada variable llamada asi
     */
    public void modificarFactura(Factura facturaSeleccionada) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(facturaSeleccionada);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo que borra una factura
     * @param facturaBorrada variable llamada asi
     */
    public void borrarFactura(Factura facturaBorrada) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(facturaBorrada);
        sesion.getTransaction().commit();
        sesion.close();
    }

    // Metodo de Repuesto

    /**
     * Metodo que da de alta a un repuesto
     * @param nuevoRepuesto variable llamada asi
     */
    public void altaRepuesto(Repuesto nuevoRepuesto) {
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoRepuesto);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Metodo que lista un repuesto
     * @return
     */
    public ArrayList<Repuesto> getRepuesto() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Repuesto ");
        ArrayList<Repuesto> lista = (ArrayList<Repuesto>)query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * Metodo que modifica un repuesto
     * @param repuestoSeleccionada variable llamada asi
     */
    public void modificarRepuesto(Repuesto repuestoSeleccionada) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(repuestoSeleccionada);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo que elimina un repuesto
     * @param repuestoBorrado variable llamada asi
     */
    public void borrarRepuesto(Repuesto repuestoBorrado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(repuestoBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    //Metodos Hoja de pedido

    /**
     * Metodo que da de alta una hoja de pedido
     * @param nuevaHoja variable llamada asi
     */
    public void altaHoja(Hojap nuevaHoja) {
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevaHoja);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Metodo que lista una hoja de pedido
     * @return
     */
    public ArrayList<Hojap> getHoja() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Hojap ");
        ArrayList<Hojap> lista = (ArrayList<Hojap>)query.getResultList();
        sesion.close();
        return lista;
    }

    /**
     * Metodo que borra una hoja de pedido
     * @param hojap variable llamada asi
     */
    public void borrarHoja(Hojap hojap){
        Session sesion = HibernateUtil.getCurrentSession();
        sesion.beginTransaction();
        sesion.delete(hojap);
        sesion.getTransaction().commit();
    }

    /**
     * Metodo que modifica una hoja de pedido
     * @param hojaSeleccionada variable llamada asi
     */
    public void modificarhoja(Hojap hojaSeleccionada) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(hojaSeleccionada);
        sesion.getTransaction().commit();
        sesion.close();
    }

}
