package com.mariava.taller.gui;

import com.mariava.taller.Clases.Factura;
import com.mariava.taller.Clases.Mecanico;
import com.mariava.taller.Clases.Vehiculo;
import com.mariava.taller.Factura.JasperReport;
import com.mariava.taller.VistaUsuarios.VistaMecanico;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;

/**
 * Clase que genera todos los metodos de la vista mecanico
 */
public class Controlador2 implements ActionListener, ListSelectionListener, FocusListener, ChangeListener {

    // Atributos
    private VistaMecanico vistaMecanico;
    private Modelo modelo;

    /**
     * Generacion de un constructor de la clase
     * @param vistaMecanico variable llamada asi, referente a la clase vista mecanico
     * @param modelo variable llamada asi, referente a la clase modelo
     */
    public Controlador2(VistaMecanico vistaMecanico, Modelo modelo) {
        this.vistaMecanico = vistaMecanico;
        this.modelo = modelo;

        addChangeListeners2(this);
        addActionListeners(this);
        addListSelectionListener(this);
    }

    /**
     * Añado listener para que al pulsar sobre cada pestaña se carguen los datos referentes
     * @param controlador2
     */
    private void addChangeListeners2(Controlador2 controlador2) {
        vistaMecanico.tabbedPane1.addChangeListener(controlador2);
    }

    /**
     *Añado los listener de todas las listas sobre las que puedo seleccionar elementos
     * @param controlador
     */
    private void addListSelectionListener(Controlador2 controlador) {
        vistaMecanico.listaMecanico.addListSelectionListener(controlador);
        vistaMecanico.listaVehiculo.addListSelectionListener(controlador);
        vistaMecanico.listarFactura.addListSelectionListener(controlador);
    }

    /**
     * Añado los listeneres de todos los botones
     * @param listener
     */
    private void addActionListeners(Controlador2 listener) {
        //Vista Mecanico
        vistaMecanico.salirItem.addActionListener(listener);
        vistaMecanico.registrarMecanicoButton.addActionListener(listener);
        vistaMecanico.modificarMecanicoButton.addActionListener(listener);
        vistaMecanico.eliminarMecanicoButton.addActionListener(listener);
        vistaMecanico.listarMecanicoButton.addActionListener(listener);

        // Vehiculo
        vistaMecanico.registrarVehiculoButton.addActionListener(listener);
        vistaMecanico.modificarVehiculoButton.addActionListener(listener);
        vistaMecanico.eliminarVehiculoButton.addActionListener(listener);
        vistaMecanico.listarVehiculoButton.addActionListener(listener);

        //Factura
//        vista.registrarFacturaButton.addActionListener(listener);
        vistaMecanico.modificarFacturaButton.addActionListener(listener);
        vistaMecanico.eliminarFacturaButton.addActionListener(listener);
        vistaMecanico.listarFacturaButton.addActionListener(listener);
        vistaMecanico.generarFacturaButton.addActionListener(listener);
    }

    /**
     *  Metodos listeners de accion, seleccion, clic de raton, y foco
     *  Metoodos de la vista de cliente
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        if(vistaMecanico.tabbedPane1.getSelectedComponent() == vistaMecanico.panelMecanico) {
            switch (comando) {
                case "Salir":
                    modelo.desconectar();
                    System.exit(0);
                    break;

                //Mecanicos Botones
                case "RegistrarMecanico":
                    nuevoMecanicoVista();
                    vistaMecanico.txtEstado.setText("Estado: Mecanico añadido");
                    break;
                case "ModificarMecanico":
                    modificarMecanicoVista();
                    vistaMecanico.txtEstado.setText("Estado: Mecanico modificado");
                    break;
                case "EliminarMecanico":
                    eliminarMecanicoVista();
                    vistaMecanico.txtEstado.setText("Estado: Mecanico eliminado");
                    break;
                case "ListarMecanicos":
                    listarMecanicoVista();
                    vistaMecanico.txtEstado.setText("Estado: Listado de mecanicos");
                    break;
            }
        } else if (vistaMecanico.tabbedPane1.getSelectedComponent() == vistaMecanico.panelVehiculo) {
            switch (comando) {
                case "Salir":
                    modelo.desconectar();
                    System.exit(0);
                    break;

                //Coches botones
                case "RegistrarVehiculo":
                    nuevoVehiculoVista();
                    vistaMecanico.txtEstado.setText("Estado: Vehiculo añadido");
                    break;
                case "ModificarVehiculo":
                    modificarVehiculoVista();
                    vistaMecanico.txtEstado.setText("Estado: Vehiculo modificado");
                    break;
                case "EliminarVehiculo":
                    eliminarVehiculoVista();
                    vistaMecanico.txtEstado.setText("Estado: Vehiculo eliminado");
                    break;
                case "ListarVehiculo":
                    listarVehiculosVista();
                    vistaMecanico.txtEstado.setText("Estado: Listado de vehiculos");
                    break;
            }
        } else if (vistaMecanico.tabbedPane1.getSelectedComponent() == vistaMecanico.panelFactura) {
            switch (comando) {
                case "Salir":
                    modelo.desconectar();
                    System.exit(0);
                    break;
                //Factura botones
                case "RegistrarFactura":
                    // nuevaFacturaVista();
                    vistaMecanico.txtEstado.setText("Estado: Factura añadido");
                    break;
                case "ModificarFactura":
                    modificarFacturaVista();
                    vistaMecanico.txtEstado.setText("Estado: Factura modificado");
                    break;
                case "EliminarFactura":
                    eliminarFacturaVista();
                    vistaMecanico.txtEstado.setText("Estado: Factura eliminado");
                    break;
                case "ListarFactura":
                    listarFacturaVista();
                    vistaMecanico.txtEstado.setText("Estado: Listado de factura");
                    break;
                case "Factura":
                    JasperReport.informeFactura();
                    vistaMecanico.txtEstado.setText("Estado: Generando factura de cliente");
                    break;
            }
        }
    }

    /**
     * Actualizo los campos que necesito
     * @param e
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
            if (e.getSource() == vistaMecanico.listMecanico) {
                Mecanico mecanicoSeleccionado = (Mecanico) vistaMecanico.listaMecanico.getSelectedValue();
                vistaMecanico.txtIdMec.setText(String.valueOf(mecanicoSeleccionado.getIdmec()));
                vistaMecanico.txtIdFacturaMecanico.setText(String.valueOf(mecanicoSeleccionado.getIdFac()));
                vistaMecanico.txtNombreMecanico.setText(mecanicoSeleccionado.getNombre());
                vistaMecanico.txtApellidoMecanico.setText(mecanicoSeleccionado.getApellido());
                vistaMecanico.txtDireccionMecanico.setText(mecanicoSeleccionado.getDireccion());
                vistaMecanico.txtTelefonoMecanico.setText(mecanicoSeleccionado.getTel());
                vistaMecanico.txtCostoMecanico.setText(String.valueOf(mecanicoSeleccionado.getCostoxhora()));
                vistaMecanico.txtHorasMecanico.setText(String.valueOf(mecanicoSeleccionado.getHorasTrabajadas()));
            }
            if (e.getSource() == vistaMecanico.listVehiculo) {
                Vehiculo vehiculoSeleccionado = (Vehiculo) vistaMecanico.listaVehiculo.getSelectedValue();
                vistaMecanico.txtIdVehiculo.setText(String.valueOf(vehiculoSeleccionado.getIdVehiculo()));
                vistaMecanico.txtMatriculaVehiculo.setText(vehiculoSeleccionado.getMatricula());
                vistaMecanico.txtModeloVehiculo.setText(vehiculoSeleccionado.getModelo());
                vistaMecanico.txtColorVehiculo.setText(vehiculoSeleccionado.getColor());
                vistaMecanico.datePicker.setText(String.valueOf(vehiculoSeleccionado.getFechaEnt()));
                vistaMecanico.timePicker.setText(String.valueOf(vehiculoSeleccionado.getHoraEnt()));
                vistaMecanico.txtKmVehiculo.setText(String.valueOf(vehiculoSeleccionado.getKilometros()));
                vistaMecanico.txtRfcVehiculo.setText(String.valueOf(vehiculoSeleccionado.getRFC()));
                vistaMecanico.txtMecVehiculo.setText(String.valueOf(vehiculoSeleccionado.getMec()));
            }
            if (e.getSource() == vistaMecanico.listFactura) {
                Factura facturaSeleccionada = (Factura) vistaMecanico.listarFactura.getSelectedValue();
                vistaMecanico.txtIdFactura.setText(String.valueOf(facturaSeleccionada.getIdFactura()));
                vistaMecanico.txtIDClienteFactura.setText(String.valueOf(facturaSeleccionada.getIdCliente()));
                vistaMecanico.txtIdHojaFactura.setText(String.valueOf(facturaSeleccionada.getIdHoja()));
                vistaMecanico.txtIdMecFactura.setText(String.valueOf(facturaSeleccionada.getIdMec()));
                vistaMecanico.txtIdVehiculoFactura.setText(String.valueOf(facturaSeleccionada.getIdVehiculo()));

                vistaMecanico.txtNumFactura.setText(String.valueOf(facturaSeleccionada.getNofact()));
                vistaMecanico.datePickerFactura.setText(String.valueOf(facturaSeleccionada.getFecha()));
                vistaMecanico.txtImpuestoFactura.setText(String.valueOf(facturaSeleccionada.getImpuesto()));
                vistaMecanico.txtRfcFactura.setText(String.valueOf(facturaSeleccionada.getRfc()));
                vistaMecanico.txtNombreFactura.setText(facturaSeleccionada.getNombreCliente());
                vistaMecanico.txtDniFacura.setText(facturaSeleccionada.getDni());
                vistaMecanico.txtTotalFactura.setText(String.valueOf(facturaSeleccionada.getTotal()));
            }
        }
    }

    /**
     * Cargos las facturas en la lista de la seccion facturas
     */
    private void listarFacturaVista() {
        ArrayList<Factura> lista = modelo.getFactura();
        vistaMecanico.dlmFactura.clear();
        for (Factura unaFactura : lista) {
            vistaMecanico.dlmFactura.addElement(unaFactura);
        }
    }

    /**
     * Elimino las facturas
     */
    private void eliminarFacturaVista() {
        Factura facturaBorrada = (Factura) vistaMecanico.listarFactura.getSelectedValue();
        modelo.borrarFactura(facturaBorrada);
    }

    /**
     * Modifico las facturas de la vista de mecanico
     */
    private void modificarFacturaVista() {
        Factura facturaSaleccionada = (Factura) vistaMecanico.listarFactura.getSelectedValue();
        facturaSaleccionada.setNombreCliente(vistaMecanico.txtNombreFactura.getText());
        facturaSaleccionada.setDni(vistaMecanico.txtDniFacura.getText());
        modelo.modificarFactura(facturaSaleccionada);
    }

    /**
     * Cargos los vehiculos en la lista de la seccion vehiculos de la vista de mecancios
     */
    private void listarVehiculosVista() {
        ArrayList<Vehiculo> lista = modelo.getVehiculo();
        vistaMecanico.dlmVehiculo.clear();
        for (Vehiculo unVehiculo : lista) {
            vistaMecanico.dlmVehiculo.addElement(unVehiculo);
        }
    }

    /**
     * Elimino vehiculos
     */
    private void eliminarVehiculoVista() {
        Vehiculo vehiculoBorrado = (Vehiculo) vistaMecanico.listaVehiculo.getSelectedValue();
        modelo.borrarVehiculo(vehiculoBorrado);
    }

    /**
     * Modifico vehiculos de la vista mecancios
     */
    private void modificarVehiculoVista() {
        Vehiculo cocheSeleccion = (Vehiculo) vistaMecanico.listaVehiculo.getSelectedValue();
        cocheSeleccion.setMatricula(vistaMecanico.txtMatriculaVehiculo.getText());
        modelo.modificarVehiculo(cocheSeleccion);
    }

    /**
     * Creo nuevos vehiculos en la partye de mecanicos
     */
    private void nuevoVehiculoVista() {
        Vehiculo nuevoVehiculo = new Vehiculo();
        nuevoVehiculo.setIdVehiculo(Integer.parseInt(vistaMecanico.txtIdVehiculo.getText()));
        nuevoVehiculo.setMatricula(vistaMecanico.txtMatriculaVehiculo.getText());
        nuevoVehiculo.setModelo(vistaMecanico.txtModeloVehiculo.getText());
        nuevoVehiculo.setColor(vistaMecanico.txtColorVehiculo.getText());
        nuevoVehiculo.setFechaEnt(Date.valueOf(vistaMecanico.datePicker.getDate()));
        nuevoVehiculo.setHoraEnt(Time.valueOf(vistaMecanico.timePicker.getTime()));
        nuevoVehiculo.setKilometros(Integer.parseInt(vistaMecanico.txtKmVehiculo.getText()));
        nuevoVehiculo.setRFC(Integer.parseInt(vistaMecanico.txtRfcVehiculo.getText()));
        nuevoVehiculo.setMec(Integer.parseInt(vistaMecanico.txtMecVehiculo.getText()));
        modelo.altaVehiculo(nuevoVehiculo);
    }

    /**
     * Listar mecanico vista
     */
    private void listarMecanicoVista() {
        ArrayList<Mecanico> lista = modelo.getMecanico();
        vistaMecanico.dlmMecanico.clear();
        for (Mecanico unMecanico : lista) {
            vistaMecanico.dlmMecanico.addElement(unMecanico);
        }
    }

    /**
     * elimino los mecanicos
     */
    private void eliminarMecanicoVista() {
        Mecanico mecanicoBorrado = (Mecanico) vistaMecanico.listaMecanico.getSelectedValue();
        modelo.borrarMecanico(mecanicoBorrado);
    }

    /**
     * Modifico los mecanicos del perfil mecanico
     */
    private void modificarMecanicoVista() {
        Mecanico mecanicoSeleccion = (Mecanico) vistaMecanico.listaMecanico.getSelectedValue();
        mecanicoSeleccion.setApellido(vistaMecanico.txtApellidoMecanico.getText());
        modelo.modificarMecanico(mecanicoSeleccion);
    }

    /**
     * Genero nuevos mecanicos en la parte de mecanicos
     */
    private void nuevoMecanicoVista() {
        Mecanico nuevoMecanico = new Mecanico();
        nuevoMecanico.setIdmec(Integer.parseInt(vistaMecanico.txtIdMec.getText()));
        nuevoMecanico.setIdFac(Integer.parseInt(vistaMecanico.txtIdFacturaMecanico.getText()));
        nuevoMecanico.setNombre(vistaMecanico.txtNombreMecanico.getText());
        nuevoMecanico.setApellido(vistaMecanico.txtApellidoMecanico.getText());
        nuevoMecanico.setDireccion(vistaMecanico.txtDireccionMecanico.getText());
        nuevoMecanico.setTel(vistaMecanico.txtTelefonoMecanico.getText());
        nuevoMecanico.setCostoxhora(Integer.parseInt(vistaMecanico.txtCostoMecanico.getText()));
        nuevoMecanico.setMatriculaCoche(vistaMecanico.txtMatriculaMecanico.getText());
        modelo.altaMecanico(nuevoMecanico);
    }


    // Metodos que no uso, pero es necesario implementarlos
    @Override
    public void focusGained(FocusEvent e) {

    }

    @Override
    public void focusLost(FocusEvent e) {

    }

    @Override
    public void stateChanged(ChangeEvent e) {

    }
}
