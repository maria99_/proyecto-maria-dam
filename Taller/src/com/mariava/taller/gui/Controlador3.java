package com.mariava.taller.gui;

import com.mariava.taller.Clases.Cliente;
import com.mariava.taller.Factura.JasperReport;
import com.mariava.taller.VistaUsuarios.VistaCliente;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;

/**
 * Clase que genera todos los metodos de la vista cliente
 */
public class Controlador3 implements ActionListener, ListSelectionListener, FocusListener, ChangeListener {

    // Atributos
    private VistaCliente vistaCliente;
    private Modelo modelo;

    /**
     * Generacion de un constructor de la clase
     *
     * @param vistaCliente variable llamada asi, referente a la clase vista cliente
     * @param modelo       variable llamada asi, referente a la clase modelo
     */
    public Controlador3(VistaCliente vistaCliente, Modelo modelo) {
        this.vistaCliente = vistaCliente;
        this.modelo = modelo;

        addChangeListeners(this);
        addActionListeners(this);
        addListSelectionListener(this);
    }


    /**
     * Añado listener para que al pulsar sobre cada pestaña se carguen los datos referentes
     *
     * @param controlador3
     */
    private void addChangeListeners(Controlador3 controlador3) {
        vistaCliente.tabbedPane1.addChangeListener(controlador3);
    }

    /**
     * Añado los listener de todas las listas sobre las que puedo seleccionar elementos
     *
     * @param listener
     */
    private void addActionListeners(Controlador3 listener) {
        //Menu
        vistaCliente.salirItem.addActionListener(listener);

        //Factura
        vistaCliente.generarFacturaClienteButton.addActionListener(listener);

        //Cliente
        vistaCliente.modificarClienteButton.addActionListener(listener);
        vistaCliente.listarClienteButton.addActionListener(listener);
    }

    /**
     * Añado los listeneres de todos los botones
     *
     * @param listener
     */
    private void addListSelectionListener(Controlador3 listener) {
        vistaCliente.listCliente.addListSelectionListener(listener);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        if (vistaCliente.tabbedPane1.getSelectedComponent() == vistaCliente.panelInforme) {
            switch (comando) {
                case "Salir":
                    modelo.desconectar();
                    System.exit(0);
                    break;

                case "Factura":
                    JasperReport.informeFactura();
                    vistaCliente.txtEstado.setText("Estado: Generando factura de cliente");
                    break;

            }
        } else if (vistaCliente.tabbedPane1.getSelectedComponent() == vistaCliente.panelCliente) {
            switch (comando) {
                //Menu de opciones
                case "Salir":
                    modelo.desconectar();
                    System.exit(0);
                    break;

                //Clientes botones
                case "ModificarClientes":
                    modificarCliente();
                    vistaCliente.txtEstado.setText("Estado: Cliente Modificado");
                    break;

                case "ListarClientes":
                    listarClientes();
                    vistaCliente.txtEstado.setText("Estado: Listado de clientes");
                    break;
            }
        }
    }


    @Override
    public void valueChanged(ListSelectionEvent e) {

    }

    /**
     * Cargos los clientes en la lista de la seccion clientes
     */
    private void listarClientes() {
        ArrayList<Cliente> lista = modelo.getCliente();
        vistaCliente.dlmCliente.clear();
        for (Cliente unCliente : lista) {
            vistaCliente.dlmCliente.addElement(unCliente);
        }
    }

    /**
     * Modifico los clientes
     */
    private void modificarCliente() {
        Cliente clienteSeleccion = (Cliente) vistaCliente.listCliente.getSelectedValue();
        clienteSeleccion.setNombre(vistaCliente.txtNombreCliente.getText());
        clienteSeleccion.setDni(vistaCliente.txtDniCliente.getText());
        modelo.modificarCliente(clienteSeleccion);
    }


    // Metodoas que no uso, pero son necesarios implementarlos
    @Override
    public void focusGained(FocusEvent e) {

    }

    @Override
    public void focusLost(FocusEvent e) {

    }

    @Override
    public void stateChanged(ChangeEvent e) {

    }


}
