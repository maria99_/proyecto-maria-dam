package com.mariava.taller.Vista;

import com.github.lgooddatepicker.components.DatePicker;
import com.github.lgooddatepicker.components.TimePicker;
import com.mariava.taller.Clases.*;

import javax.swing.*;

/**
 * Clase donde se ven todas las pestañas de la aplicaion y las opciones
 */
public class VistaVentana extends JFrame {
    private JFrame frame;
    public JPanel panel;
    public JTabbedPane tabbedPane1;

    //Etiqueta estado
    public JLabel txtEstado;

    //Menu con opciones diferentes
    public JMenuItem salirItem;

    //Clientes
    public JPanel panelClientes;
    public JTextField txtIdCliente;
    public JTextField txtRfcCliente;
    public JTextField txtIdUsuarioCliente;
    public JTextField txtNombreCliente;
    public JTextField txtDireccionCliente;
    public JTextField txtTelefonoCliente;
    public JTextField txtCodigoPostal;
    public JTextField txtPoblacionCliente;
    public JTextField txtDNICliente;
    public JTextField txtEmailCliente;
    public JButton registrarClienteButton;
    public JButton modificarClienteButton;
    public JButton eliminarClienteButton;
    public JButton listarClienteButton;

    public JList listarClientes;
    public JList<Cliente> listCliente;
    public DefaultListModel<Cliente> dlmClientes;

    //Mecanicos
    public JPanel panelMecanicos;
    public JTextField txtIdMecanico;
    public JTextField txtNombreMecanico;
    public JTextField txtApellidoMecanico;
    public JTextField txtDireccionMecanico;
    public JTextField txtTelefonoMecanico;
    public JTextField txtCostoMecanico;
    public JTextField txtMatriculaMecanico;
    public JTextField txtHorasMecanico;
    public JButton registrarMecanicoButton;
    public JButton modificarMecanicoButton;
    public JButton eliminarMecanicoButton;
    public JButton listarMecanicoButton;
    public JTextField txtIdFacMecanico;

    public JList listarMecanico;
    public JList<Mecanico> listMecanico;
    public DefaultListModel<Mecanico> dlmMecanico;

    //Vehiculo
    public JPanel panelVehiculos;
    public JTextField txtIdVehiculo;
    public JTextField txtMatriculaVehiculo;
    public JTextField txtModeloVehiculo;
    public JTextField txtColorVehiculo;
    public JTextField txtKmVehiculo;
    public DatePicker datePicker;
    public TimePicker timePicker;
    public JButton registrarVehiculoButton;
    public JButton modificarVehiculoButton;
    public JButton eliminarVehiculoButton;
    public JButton listarVehiculoButton;
    public JTextField txtRfcVehiculo;
    public JTextField txtMecVehiculo;

    public JList listarVehiculos;
    public JList<Vehiculo> listVehiculo;
    public DefaultListModel<Vehiculo> dlmVehiculo;


    //Factura
    public JPanel panelFactura;
    public JTextField txtIdFactura;
    public JTextField txtNumeroFactura;
    public DatePicker datePickerFactura;
    public JTextField txtImpuestoFactura;
    public JTextField txtRfcFactura;
    public JTextField txtNombreFactura;
    public JTextField txtDniFactura;
    public JTextField txtTotalFactura;
    public JButton registrarFacturaButton;
    public JButton modificarFacturaButton;
    public JButton eliminarFacturaButton;
    public JButton listarFacturaButton;
    public JButton generarFacturaButton;
    public JTextField txtIdClienteFactura;
    public JTextField txtIdHojaFactura;
    public JTextField txtIdMecFactura;
    public JTextField txtIdVehiculoFactura;

    public JList listarFactura;
    public JList<Factura> listFactura;
    public DefaultListModel<Factura> dlmFactura;

    //Repuesto
    public JPanel panelRepuesto;
    public JTextField txtIdRepuesto;
    public JTextField txtDescripcionReo;
    public JTextField txtCostoRepuesto;
    public JTextField txtPrecioRepuesto;
    public JTextField txtNombreRepuesto;
    public JTextField txtCantidadRepuesto;
    public JTextField txtSolicitanteRepuesto;
    public JTextField txtProveedorRepuesto;
    public JButton registrarRepuestoButton;
    public JButton modificarRepuestoButton;
    public JButton eliminarRepuestoButton;
    public JButton listarRepuestoButton;
    public JButton graficoPiezasButton;
    public JTextField txtidHojaRepuesto;

    public JList listarRepuesto;
    public JList<Repuesto> listRepuesto;
    public DefaultListModel<Repuesto> dlmRepuesto;

    //Hoja Pedido
    public JPanel panelHoja;
    public JTextField txtIdHoja;
    public JTextField txtIdFacturaHoja;
    public JTextField txtConceptoHoja;
    public JTextField txtCantidadHoja;
    public JTextField txtSolicitanteHoja;
    public JTextField txtNombreHoja;
    public DatePicker datePickerHoja;
    public JComboBox comboBoxPiezaHoja;
    public JButton modificarHojaButton;
    public JButton eliminarHojaButton;
    public JButton listarHojaButton;
    public JTextField txtIdMecHoja;
    public JTextField txtRepHoja;

    public JList listarHoja;
    public JList<Hojap> listHoja;
    public DefaultListModel<Hojap> dlmHoja;
    public DefaultComboBoxModel<Hojap> dcbmHoja;

    /**
     * Creacion del constructor con el tamaño de la ventana e inicializacion de elementos
     */
    public VistaVentana(){
        frame = new JFrame("TALLER S.L");
        frame.setContentPane(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        //Clientes
        registrarClienteButton.setToolTipText("Registrar un cliente");
        modificarClienteButton.setToolTipText("Solo puedes modificar nombre y dni");
        eliminarClienteButton.setToolTipText("Eliminar a un cliente");
        listarClienteButton.setToolTipText("Listar a los clientes");

        //Mecanicos
        registrarMecanicoButton.setToolTipText("Registrar un mecanico");
        modificarMecanicoButton.setToolTipText("Solo puedes modificar el apellido");
        eliminarMecanicoButton.setToolTipText("Eliminar a un mecanico");
        listarMecanicoButton.setToolTipText("Listar a los mecanicos");

        //Vehiculos
//        registrarVehiculoButton.setToolTipText("Registrar un vehiculo");
        modificarVehiculoButton.setToolTipText("Solo puedes modificar la matricula");
        eliminarVehiculoButton.setToolTipText("Eliminar a un vehiculo");
        listarVehiculoButton.setToolTipText("Listar a los vehiculos");

        //Factura
       // registrarFacturaButton.setToolTipText("Registrar una factura");
        modificarFacturaButton.setToolTipText("Puedes modificar nombre y dni");
        eliminarFacturaButton.setToolTipText("Eliminar una factura");
        listarFacturaButton.setToolTipText("Listar las facturas");
        generarFacturaButton.setToolTipText("Generar la factura de un cliente");

        //Repuestos
        registrarRepuestoButton.setToolTipText("Registrar un repuesto");
        modificarRepuestoButton.setToolTipText("Solo puedes modificar el nombre de la pieza");
        eliminarRepuestoButton.setToolTipText("Eliminar un repuesto");
        listarRepuestoButton.setToolTipText("Listar los respuestos");
        graficoPiezasButton.setToolTipText("Indica la cantidad de piezas de cada repuesto");

        //Hoja pedido
      //  registrarHojaButton.setToolTipText("Registrar una hoja de pedido");
        modificarHojaButton.setToolTipText("Solo puedes modificar el el concepto");
        eliminarHojaButton.setToolTipText("Eliminar una hoja de pedido");
        listarHojaButton.setToolTipText("Listar las hojas de pedido");

        crearMenu();

        iniciarComboBox();

        iniciarListaCliente();
        inciarListaMecanicos();
        iniciarListaVehiculo();
        iniciarListaFactura();
        iniciarListaRepuesto();
        inicarListaHoja();
    }

    /**
     * Iniciar la lista de hoja de pedido
     */
    private void inicarListaHoja() {
        dlmHoja = new DefaultListModel<>();
        listarHoja.setModel(dlmHoja);
    }

    /**
     * Iniciar la lista de repuesto
     */
    private void iniciarListaRepuesto() {
        dlmRepuesto = new DefaultListModel<>();
        listarRepuesto.setModel(dlmRepuesto);
    }

    /**
     * Iniciar la lista de factura
     */
    private void iniciarListaFactura() {
        dlmFactura = new DefaultListModel<>();
        listarFactura.setModel(dlmFactura);
    }

    /**
     * Iniciar la lista de vehiculo
     */
    private void iniciarListaVehiculo() {
        dlmVehiculo = new DefaultListModel<>();
        listarVehiculos.setModel(dlmVehiculo);
    }

    /**
     * Iniciar la lista de mecanico
     */
    private void inciarListaMecanicos() {
        dlmMecanico = new DefaultListModel<>();
        listarMecanico.setModel(dlmMecanico);
    }

    /**
     * Iniciar la lista de cliente
     */
    private void iniciarListaCliente() {
        dlmClientes = new DefaultListModel<>();
        listarClientes.setModel(dlmClientes);
    }

    /**
     * Iniciar combobox a utilizar
     */
    private void iniciarComboBox() {
        dcbmHoja = new DefaultComboBoxModel<>();
        comboBoxPiezaHoja.setModel(dcbmHoja);
    }

    /**
     * Creacion del menu de opciones, con sus items
     */
    private void crearMenu() {
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Opciones");

        salirItem = new JMenuItem("Salir");
        salirItem.setActionCommand("Salir");

        menu.add(salirItem);
        barra.add(menu);
        frame.setJMenuBar(barra);

    }


}
