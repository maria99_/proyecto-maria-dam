package com.mariava.taller.Vista;

import com.mariava.taller.Util.HibernateUtil;
import com.mariava.taller.VistaUsuarios.VistaCliente;
import com.mariava.taller.VistaUsuarios.VistaMecanico;
import com.mariava.taller.gui.Controlador;
import com.mariava.taller.gui.Controlador2;
import com.mariava.taller.gui.Controlador3;
import com.mariava.taller.gui.Modelo;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Clase donde haces el login de la aplicacion
 */
public class Login extends JFrame {

    //Atributos de la clase
    private JPanel panel1;
    private JFrame frame;

    private JButton aceptarButton;
    private JButton cancelarButton;
    private JTextField txtUsuario;

    /**
     * Costructor, con tamaño de ventana y metodos
     */
    public Login(){
        frame = new JFrame("Ingreso al sistema");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        aceptarButton.setToolTipText("Entrar como cliente, administrador, mecanico");
        cancelarButton.setToolTipText("Sale de la aplicacion");

        /**
         * Metodo que cuando hagas click, puedas acceder al resto de la aplicacion o
         * te de error, si no estas registrado
         */
        aceptarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if(txtUsuario.getText().equals("cliente")){
                    System.out.println("entrando como cliente");
                    VistaCliente cliente = new VistaCliente();
                    Modelo modelo = new Modelo();
                    modelo.conectar();
                    Controlador3 controlador3 = new Controlador3(cliente, modelo);
                }

                if(txtUsuario.getText().equals("mecanico")){
                    System.out.println("entrando como mecanico");
                    VistaMecanico mecanico = new VistaMecanico();
                    Modelo modelo = new Modelo();
                    modelo.conectar();
                    Controlador2 controlador2 = new Controlador2(mecanico, modelo);
                }
                if(txtUsuario.getText().equals("administrador")){
                    System.out.println("entrando como administrador");
                    Modelo modelo = new Modelo();
                    VistaVentana vista = new VistaVentana();
                    modelo.conectar();
                    Controlador controlador = new Controlador(vista, modelo);
                }
            }
        });

        /**
         * Metodo que sale directamente del programa
         */
        cancelarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Salir");
                System.exit(0);

            }
        });

    }
}
